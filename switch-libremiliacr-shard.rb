#!/usr/bin/env ruby
require "optparse"
require "yaml"

URL = "https://chiselapp.com/user/MistressRemilia/repository/libremiliacr"

args = {:latest => false, :commit => nil, :version => nil}
OptionParser.new do |opts|
  opts.banner = "Usage: switch-shard.rb --latest
Usage: switch-shard.rb --commit <commit ID>
Usage: switch-shard.rb --version <version>"

  opts.on("-l", "--latest", "Use latest version (path: ../libremilia)") do |_|
    args[:latest] = true
  end

  opts.on("-cCOMMIT", "--commit=COMMIT", "Use specific commit") do |v|
    args[:commit] = v
  end

  opts.on("-vVERSION", "--version=VERSION", "Use version specifier") do |v|
    args[:version] = v
  end
end.parse!

data = YAML.load_file("shard.yml")

if args[:latest]
  abort "Cannot combine options" if args[:commit] || args[:version]
  data["dependencies"]["libremiliacr"]["path"] = "../libremiliacr"
  data["dependencies"]["libremiliacr"].delete("fossil")
  data["dependencies"]["libremiliacr"].delete("commit")
  data["dependencies"]["libremiliacr"].delete("version")
elsif args[:commit]
  abort "Cannot combine options" if args[:latest] || args[:version]
  data = YAML.load_file("shard.yml")
  data["dependencies"]["libremiliacr"].delete("path")
  data["dependencies"]["libremiliacr"]["fossil"] = URL
  data["dependencies"]["libremiliacr"]["commit"] = args[:commit]
  data["dependencies"]["libremiliacr"].delete("version")
elsif args[:version]
  abort "Cannot combine options" if args[:commit] || args[:latest]
  data["dependencies"]["libremiliacr"].delete("path")
  data["dependencies"]["libremiliacr"]["fossil"] = URL
  data["dependencies"]["libremiliacr"].delete("commit")
  data["dependencies"]["libremiliacr"]["version"] = args[:version]
end

puts data.to_yaml
