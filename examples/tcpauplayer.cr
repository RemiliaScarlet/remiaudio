####
#### Au File Test Player.
####
#### This is an extremly bare bones player.
####
require "../src/remiaudio/drivers"
require "../src/remiaudio/formats"

if ARGV.empty?
  STDERR << "No file specified\n"
  exit 1
end

@[AlwaysInline]
def fillBuf(buf : Array(Float32), data : RemiAudio::SampleData, srcFormat)
  RemiAudio::DSP::Ditherer.withConvertedSamples(data, srcFormat, RemiAudio::SampleFormat::F32) do |samp, i|
    buf[i] = samp.to_f32!
  end
end

# Open the file stream.
au = RemiAudio::Formats::AuFile.open(ARGV[0])
channels = au.stereo? ? 2 : 1
buf64 = Array(Float64).new(4410, 0.0)
buf = Array(Float32).new(4410, 0.0)
numRead : Int32 = 0

format("Initializing audio...~%(output: 32-bit IEEE Float, little endian, ~d channel~:p, ~d Hz)~%",
       channels, au.sampleRate)
strm = RemiAudio::Drivers::Tcp::TcpDevice.new(au.sampleRate, :F32, channels, host: "localhost", port: 6969u16)
strm.ditherer = RemiAudio::DSP::Ditherer.new
begin
  strm.bufferSize = buf.size.tdiv(channels)
  puts "Attempting to connect to localhost:6969..."
  strm.start

  # Print some nice info.
  puts "\nSample Rate: #{au.sampleRate}"
  puts "Stereo?: #{au.stereo?}"
  puts "Au Note: #{au.note || ""}"

  # Playback audio until done.
  while (numRead = au.read(buf64)) != 0
    if numRead < buf64.size
      buf64.fill(0.0, numRead..)
    end
    buf = buf64.map(&.to_f32!)
    strm << buf
  end
  sleep 1.second
ensure
  strm.stop
end
