####
#### WAV -> AU Example Converter.
####
#### This is extremly bare bones player.
####
require "../src/remiaudio/formats"

if ARGV.size != 2
  STDERR << "Usage: #{File.basename(PROGRAM_NAME)} <WAV File> <Au File>\n"
  exit 1
end

abort "Input file and output file cannot be the same" if Path[ARGV[0]].expand == Path[ARGV[1]].expand

RemiAudio::Formats::WavFile.open(ARGV[0]) do |wav|
  unless wav.channels == 1 || wav.channels == 2
    STDERR << "Unsupported number of WAV channels (must be 1 or 2)\n"
    exit 1
  end

  destEnc = case wav.encoding
            when .lpcm?
              case wav.bitDepth
              when 16 then RemiAudio::Formats::AuEncoding::Lpcm16bit
              when 24 then RemiAudio::Formats::AuEncoding::Lpcm24bit
              when 32 then RemiAudio::Formats::AuEncoding::Lpcm32bit
              else abort "This program does not support #{wav.encoding} #{wav.bitDepth}-bit WAVEs"
              end
            when .float?
              case wav.bitDepth
              when 32 then RemiAudio::Formats::AuEncoding::Float32
              when 64 then RemiAudio::Formats::AuEncoding::Float64
              else abort "This program does not support #{wav.encoding} #{wav.bitDepth}-bit WAVEs"
              end
            else abort "This program does not support #{wav.encoding} WAVEs"
            end

  RemiAudio::Formats::AuFile.create(ARGV[1], sampleRate: wav.sampleRate.to_u32,
                                    encoding: destEnc,
                                    note: "Converted with wav2au",
                                    channels: wav.channels) do |au|
    loop do
      begin
        au << wav.read
      rescue IO::EOFError
        break
      end
    end
  end
end

puts "Au file written: #{File.size(ARGV[1]).prettySize}"
