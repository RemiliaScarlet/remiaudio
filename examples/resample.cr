####
#### Basic Resampler
####
#### Reads a WAV or Au entirely into RAM, resamples it to the desired output
#### rate, then writes the result as a WAV to the given output file.
####
#### The type of resampler can be selected.  If it's not given, then it defaults
#### to linear resampling.
####
#### This will not clobber existing files.  If the input file is already at the
#### chosen sample rate, this does nothing.
require "../src/remiaudio"

# For shorter typing
alias Fmt = RemiAudio::Formats

# Check argument count
if ARGV.size < 3 || ARGV.size > 4
  abort("Usage: #{Path[PROGRAM_NAME].basename} <input file> <output file> <target sample rate> [resampler]")
end

# Parse most of the command line
inFilename : String = ARGV[0]
outFilename : String = ARGV[1]
targetRate : UInt32 = ARGV[2].to_u32? || abort("Bad sample rate")

# Check files
abort("Input file does not exist") unless File.exists?(inFilename)
abort("Will not clobber existing files") if File.exists?(outFilename)

# Read the source file entirely into RAM
puts "Reading source file..."
srcData : Array(Float64) = [] of Float64
chan : UInt32 = 0u32
sourceRate : UInt32 = 0u32
Fmt::AudioFile.open(inFilename) do |file|
  srcData = file.readToEnd
  chan = file.channels
  sourceRate = file.sampleRate
end

# Report how much we've read, and then check that we're not already at the
# target rate.
puts "Read #{srcData.size.tdiv(chan)} samples at #{sourceRate} Hz"
if sourceRate == targetRate
  abort("Input file is already at #{targetRate} Hz")
end

# Now resample the data into destData
ratio : Float64 = targetRate / sourceRate
destData = Array(Float64).new((srcData.size * ratio + 0.5).to_i32!, 0.0f32)
resamp = if ARGV.size == 3
           puts "Resampling using linear resampling..."
           RemiAudio::Resampler::LinearResampler.new(chan.to_i32)
         else
           case ARGV[3].downcase
           when "sincbest"
             abort("SincBest only supports stereo right now") unless chan == 2
             puts "Resampling using Sinc (best quality)..."
             RemiAudio::Resampler::SincResamplerStereo.new(:best)
           when "sincmedium"
             abort("SincBest only supports stereo right now") unless chan == 2
             puts "Resampling using Sinc (medium quality)..."
             RemiAudio::Resampler::SincResamplerStereo.new(:medium)
           when "sincfast"
             abort("SincBest only supports stereo right now") unless chan == 2
             puts "Resampling using Sinc (fastest quality)..."
             RemiAudio::Resampler::SincResamplerStereo.new(:fast)
           when "linear"
             puts "Resampling using linear resampling..."
             RemiAudio::Resampler::LinearResampler.new(chan.to_i32)
           when "zoh", "zeroorderhold"
             puts "Resampling using zero-order hold resampling..."
             RemiAudio::Resampler::ZohResampler.new(chan.to_i32)
           else
             abort(%|Supported resamplers:
* sincbest
* sincmedium
* sincfast
* linear
* zoh / zeroorderhold|)
           end
         end
numUsed, numWritten, _ = resamp.process(srcData, destData, ratio)
puts "Used #{numUsed} samples, wrote #{numWritten} samples"

# Write the results
Fmt::WavFile.create(outFilename, sampleRate: targetRate, channels: chan, bitDepth: 32, encoding: :Float) do |file|
  # The number written is _per channel_, so multiply by the number of channels.
  (numWritten * chan).times do |idx|
    file.write(destData[idx])
  end
end

puts "Output WAV written, size: #{File.size(outFilename).prettySize}"
