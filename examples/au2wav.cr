####
#### AU -> WAV Example Converter.
####
#### This is extremly bare bones player.
####
require "../src/remiaudio/formats"

if ARGV.size != 2
  STDERR << "Usage: #{File.basename(PROGRAM_NAME)} <Au File> <WAV File>\n"
  exit 1
end

abort "Input file and output file cannot be the same" if Path[ARGV[0]].expand == Path[ARGV[1]].expand

RemiAudio::Formats::AuFile.open(ARGV[0]) do |au|
  destEnc, destBits = case au.encoding
                      when .lpcm8bit?
                        {RemiAudio::Formats::WavEncoding::Lpcm, 8}
                      when .lpcm16bit?
                        {RemiAudio::Formats::WavEncoding::Lpcm, 16}
                      when .lpcm24bit?
                        {RemiAudio::Formats::WavEncoding::Lpcm, 24}
                      when .lpcm32bit?
                        {RemiAudio::Formats::WavEncoding::Lpcm, 32}
                      when .float32?
                        {RemiAudio::Formats::WavEncoding::Float, 32}
                      when .float64?
                        {RemiAudio::Formats::WavEncoding::Float, 64}
                      else
                        abort "Cannot use this to convert Au to WAV for this encoding: #{au.encoding}"
                      end

  RemiAudio::Formats::WavFile.create(ARGV[1],
                                     channels: au.stereo? ? 2 : 1,
                                     sampleRate: au.sampleRate,
                                     encoding: destEnc,
                                     bitDepth: destBits) do |wav|
    au.each do |smp|
      wav << smp
    end
  end
end

puts "WAV file written: #{File.size(ARGV[1]).prettySize}"
