####
#### Example of how to plot multiple BiQuadFilter instances using Gnuplot.
####
require "../src/remiaudio/dsp"

# Sampling frequency
SAMPLE_FREQ = 44100.0

# These will be turned into BiQuadFilter instances.  Add or remove as you wish.
# The mode can be: :lshelf, :hshelf, :peak, :lpass, :hpass, :bpass, :notch, or
# :apass.  Make sure all parameters are Float64.
#
# Check the function documentations for the actual meaning of the width
# parameter.
filters = [
  {mode: :lshelf, freq: 80.0,    gain:  2.4, width: 1.1},
  {mode: :peak,   freq: 105.0,   gain:  6.3, width: 0.4},
  {mode: :peak,   freq: 275.0,   gain: -9.0, width: 0.7},
  {mode: :peak,   freq: 3000.0,  gain:  3.0, width: 2.4},
  {mode: :lpass,  freq: 16000.0, width: 0.707}
]

# These are just for the makeFilter function below.  Only the shelf filters and
# peaking EQ filter takes a gain parameter.
alias FreqGainWidthDef = NamedTuple(mode: Symbol, freq: Float64, gain: Float64, width: Float64)
alias FreqWidthDef = NamedTuple(mode: Symbol, freq: Float64, width: Float64)
alias FilterDef = FreqGainWidthDef|FreqWidthDef

def makeFilter(fdef : FilterDef) : RemiAudio::DSP::BiQuadFilter
  ret = RemiAudio::DSP::BiQuadFilter.new(SAMPLE_FREQ)
  case fdef[:mode]
  when :lshelf then ret.setLowShelf( fdef[:freq], fdef.as(FreqGainWidthDef)[:gain], fdef[:width])
  when :hshelf then ret.setHighShelf(fdef[:freq], fdef.as(FreqGainWidthDef)[:gain], fdef[:width])
  when :peak   then ret.setPeakingEQ(fdef[:freq], fdef.as(FreqGainWidthDef)[:gain], fdef[:width])
  when :apass  then ret.setAllPass(  fdef[:freq], fdef[:width])
  when :notch  then ret.setNotch(    fdef[:freq], fdef[:width])
  when :bpass  then ret.setBandPass( fdef[:freq], fdef[:width])
  when :lpass  then ret.setLowPass(  fdef[:freq], fdef[:width])
  when :hpass  then ret.setHighPass( fdef[:freq], fdef[:width])
  else abort("Bad filter type: #{fdef[:mode]}")
  end
  ret
end

ret = String.build do |str|
  # Normally this is all output by the BiQuadFilter#plot method, but #plot
  # assumes you are plotting a single filter.
  str << "# gnuplot file\n"
  str << "set title 'BiQuad Filter, sample rate: #{SAMPLE_FREQ}'\n"
  str << "set xlabel 'Frequency (Hz)'\n"
  str << "set ylabel 'Amplitude Response (dB)'\n"
  str << sprintf("Fs = %g\n", 44100.0)
  str << "o = 2 * pi / Fs\n"
  str << "set logscale x\n"
  str << "set samples 250\n"
  str << "set grid xtics ytics\n"
  str << "set key off\n"

  filters.each_with_index do |filtDef, idx|
    # Create the BiQuadFilter from the definition.
    filt = makeFilter(filtDef)

    # Run the plot method.  We're only interested in the coefficients, and we're
    # going to adjust them.
    str << filt.plot(RemiAudio::PlotType::GnuPlot, coeffsOnly: true).gsub(/([ab][012])/, "\\1_#{idx}") << '\n'

    # We create a function for Gnuplot now.  Normally the #plot method does this
    # for when plotting a single filter.  But we're combining filters, so we
    # need to do it differently than #plot does.
    str << "H_#{idx}(f) = sqrt((b0_#{idx} * b0_#{idx} + b1_#{idx} * b1_#{idx} + b2_#{idx} * b2_#{idx} + 2.0 * " <<
      "(b0_#{idx} * b1_#{idx} + b1_#{idx} * b2_#{idx}) * cos(f * o) + 2.0 * " <<
      "(b0_#{idx} * b2_#{idx}) * cos(2.0 * f * o)) / " <<
      "(1.0 + a1_#{idx} * a1_#{idx} + a2_#{idx} * a2_#{idx} + 2.0 * (a1_#{idx} + a1_#{idx} * a2_#{idx}) * " <<
      "cos(f * o)+2.0 * a2_#{idx} * cos(2.0 * f * o)))\n"
  end

  # Now we make the main function.  Again, this isn't needed when you just plot
  # a single filter.
  str << "H(f) = "
  filters.size.times { |x| str << "H_#{x}(f)#{x == filters.size - 1 ? "\n" : " * "}" }

  # Finish up
  str << "plot [f=10 : Fs/2] [-35:25] 20 * log10(H(f))"
end

# Print the generated script
puts ret
