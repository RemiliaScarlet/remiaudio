####
#### Example SampleInfo Usage
####
require "../src/remiaudio/formats"

# Check arguments
unless ARGV.size == 1
  abort "Usage: #{PROGRAM_NAME} <file>"
end

begin
  info = RemiAudio::Formats::SampleInfo.from(ARGV[0])
  if info.rmsPerChan.size != 2
    abort("Only stereo files are supported")
  end

  minLeft  = sprintf("%0.2f   ", info.minPerChan[0])
  minRight = sprintf("%0.2f   ", info.minPerChan[1])
  min      = sprintf("%0.2f   ", info.min)

  maxLeft  = sprintf("%0.2f   ", info.maxPerChan[0])
  maxRight = sprintf("%0.2f   ", info.maxPerChan[1])
  max      = sprintf("%0.2f   ", info.max)

  peakLeft  = sprintf("%0.2f dB", info.peakPerChan[0])
  peakRight = sprintf("%0.2f dB", info.peakPerChan[1])
  peak      = sprintf("%0.2f dB", info.peak)

  rmsLeft  = sprintf("%0.2f dB", info.rmsPerChan[0])
  rmsRight = sprintf("%0.2f dB", info.rmsPerChan[1])
  rms      = sprintf("%0.2f dB", info.rms)

  length = Time::Span.new(nanoseconds: ((info.samplesPerChan / info.sampleRate) * 1000000000).to_i64!)

  format("Total samples: ~:d (~a CD frames)~%", info.numSamples, sprintf("%0.2f", info.numSamples / 588))
  format("Samples per channel: ~:d~%", info.samplesPerChan)
  puts "Length: #{length}"
  puts ""
  puts   "              Left        Right      Overall"
  printf("Peak: %12s %12s %12s\n", peakLeft, peakRight, peak)
  printf("Min:  %12s %12s %12s\n", minLeft, minRight, min)
  printf("Max:  %12s %12s %12s\n", maxLeft, maxRight, max)
  printf("RMS:  %12s %12s %12s\n", rmsLeft, rmsRight, rms)
rescue err : RemiAudio::Formats::SampleInfo::FormatError
  abort("[Error]: #{err}")
end
