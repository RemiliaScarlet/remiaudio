####
#### Generates a 4-second long sine wave and writes it to the given Au file.
#### The sine is at half volume.
####
require "../src/remiaudio/formats/au"

abort("No file specified") unless ARGV.size == 1
abort("Will not clobber existing files") if File.exists?(ARGV[0])

srate = 44100u32
length = 4
au = RemiAudio::Formats::AuFile.new(sampleRate: srate)
(srate * length).times do |idx|
  smp = Math.sin(2.0 * Math::PI * 440.0 * idx / 44100) * 0.5
  au.write(smp)
  au.write(smp)
end

File.open(ARGV[0], "wb") do |f|
  au.copyTo(f)
end

puts "File written"
