require "../src/remiaudio"

def playBinaryTrack(file, trk, bigEndian : Bool = false)
  # Audio buffer.  The second is just a Slice(UInt8) into the buffer.
  i16Buf = Array(Int16).new(1024, 0i16)
  i16Ptr = Bytes.new(i16Buf.to_unsafe.unsafe_as(Pointer(UInt8)), i16Buf.size * 2)

  # Filename
  filename = Path[Path[ARGV[0]].dirname, file.filename]

  # Timestamps and corresponding byte positions
  idxStart = trk.index(1)
  idxEnd = file.tracks[trk.trackNumber]?.try(&.index(1)) ||
           RemiAudio::Cue::Timestamp.fromBytePos(File.size(filename))
  byteStart = idxStart.toBytePos
  byteEnd = idxEnd.toBytePos

  # Play audio
  puts "Playing #{file.filename} track #{trk.trackNumber} (raw audio)"
  puts "  Track is from #{idxStart} to #{idxEnd || "End"} (#{idxEnd - idxStart})"
  puts "  This corresponds to bytes #{byteStart} to #{byteEnd}"
  RemiAudio::Drivers.withDevice(RemiAudio::Drivers::Ao::AoDevice, 44100, 16, 2) do |strm|
    strm.bufferSize = i16Buf.size.tdiv(2)
    strm.start

    # Open the file and seek to the track's position
    File.open(filename) do |infile|
      infile.pos = numRead = byteStart

      # Read track until finished.
      while numRead < byteEnd
        count = infile.read(i16Ptr)
        i16Buf.fill(0i16, count..) if count < i16Buf.size

        if bigEndian
          i16Buf.map! do |smp|
            smp.byte_swap
          end
        end

        strm << i16Buf
        break if count < i16Buf.size
        numRead += count
      end
    end
  end
rescue err : Exception
  RemiLib.log.error(err)
end

def playWavTrack(file, trk)
  # Audio bufferss
  f32Buf = Array(Float32).new(1024, 0.0f32)

  # Filename
  filename = Path[Path[ARGV[0]].dirname, file.filename]

  # Timestamps and corresponding byte positions
  idxStart = trk.index(1)
  idxEnd = file.tracks[trk.trackNumber]?.try(&.index(1)) ||
           RemiAudio::Cue::Timestamp.fromBytePos(File.size(filename))
  smpStart = idxStart.toSampleNum
  smpEnd = idxEnd.toSampleNum

  numRead = smpStart

  # Play audio
  puts "Playing #{file.filename} track #{trk.trackNumber} (WAVE audio)"
  puts "  Track is from #{idxStart} to #{idxEnd || "End"} (#{idxEnd - idxStart})"
  RemiAudio::Drivers.withDevice(RemiAudio::Drivers::Ao::AoDevice, 44100, 16, 2) do |strm|
    strm.bufferSize = f32Buf.size.tdiv(2)
    strm.start

    # Open the file and seek to the track's position
    RemiAudio::Formats::AudioFile.open(filename) do |infile|
      if infile.sampleRate != 44100 || infile.bitDepth != 16 || infile.channels != 2
        abort("File is not 44.1 Khz 16-bit stereo: #{file.filename}")
      end
      infile.pos = numRead = smpStart

      # Read track until finished.
      while numRead < smpEnd
        count = infile.read(f32Buf)
        f32Buf.fill(0.0f32, count..) if count < f32Buf.size
        strm << f32Buf
        break if count < f32Buf.size
        numRead += count * 2 # 2 for two channels
      end
    end
  end
rescue err : Exception
  RemiLib.log.error(err)
end

def playTrack(file, trk)
  if trk.type.audio?
    if trk.flags.four_channel?
      puts "Skipping #{file.filename} track #{trk.trackNumber}: can't play 4-channel audio"
    elsif file.type.binary?
      playBinaryTrack(file, trk)
    elsif file.type.wave?
      playWavTrack(file, trk)
    elsif file.type.motorola?
      playBinaryTrack(file, trk, true)
    else raise "Should have been checked earlier"
    end
  else
    puts "Skipping #{file.filename} track #{trk.trackNumber}: not audio"
  end
end

if ARGV.empty?
  abort(%|Usage: #{Path[PROGRAM_NAME].basename} <cue file> [track number]

If the track number is not specified, then all audio tracks are played
sequentially.

Only raw CD audio and WAVE files are supported|)
end

cuefile = RemiAudio::Cue.load(ARGV[0])
desired = ARGV[1]?.try(&.to_i32)
abort("No files defined in CUE") if cuefile.files.empty?

cuefile.files.each do |file|
  case file.type
  when .binary?, .wave?, .motorola?
    if desired
      playTrack(file, file.tracks[desired])
    else
      file.tracks.each do |trk|
        playTrack(file, trk)
      end
    end
  else
    puts "Skipping file in CUE: #{file.filename}"
  end
end
