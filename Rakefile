require "rake/clean"

AU_PLAYER_BIN = "bin/auplayer"
AU_TO_WAV_BIN = "bin/au2wav"
WAV_TO_AU_BIN = "bin/wav2au"
BIQUADPLOT_BIN = "bin/biquadplot"
AUDIO_STATS_BIN = "bin/audio-stats"
GEN_SINE_AU_BIN = "bin/gen-sine-au"
RESAMPLE_BIN = "bin/resample"
TCP_AU_PLAYER_BIN = "bin/tcpauplayer"
CUE_PLAYER_BIN = "bin/cueplayer"

EXAMPLE_BINARIES = Rake::FileList[
  AU_PLAYER_BIN, AU_TO_WAV_BIN, WAV_TO_AU_BIN, BIQUADPLOT_BIN, AUDIO_STATS_BIN,
  GEN_SINE_AU_BIN, RESAMPLE_BIN, TCP_AU_PLAYER_BIN, CUE_PLAYER_BIN
]

LIB_SOURCES = Rake::FileList["src/**/*.cr"]
SPEC_SOURCES = Rake::FileList["spec/*/*.cr"]
SHARD = "shard.yml"
SHARD_LOCK = "shard.lock"
LIB_DIR = "./lib"
BIN_DIR = "./bin"
DOCS_DIR = "./docs"

EXAMPLE_DEPS = LIB_SOURCES + [SHARD, SHARD_LOCK, LIB_DIR, BIN_DIR]

OPT_MODE_FAST = "fast"
OPT_MODE_3 = "3"
OPT_MODE_2 = "2"
OPT_MODE_1 = "1"
OPT_MODE_DEBUG = "debug"

directory BIN_DIR

file LIB_DIR => SHARD do |t|
  sh "shards install"
end

file SHARD_LOCK => SHARD do |t|
  sh "shards install"
end
CLOBBER << SHARD_LOCK

EXAMPLE_BINARIES.each do |bin|
  CLEAN << bin
end

CLOBBER << BIN_DIR
CLOBBER << LIB_DIR

def buildExample(t, args, optMode)
  cmdLine = "crystal build -p -o #{t.name}"

  if optMode == OPT_MODE_FAST
    cmdLine += " --release --no-debug"
  elsif optMode == OPT_MODE_3
    cmdLine += " -O3 --debug"
  elsif optMode == OPT_MODE_2
    cmdLine += " -O2 --debug"
  elsif optMode == OPT_MODE_1
    cmdLine += " -O1 --debug"
  elsif optMode == OPT_MODE_DEBUG
    cmdLine += " -Dremiaudio_extrachecks -Dremiaudio_debug"
  elsif optMode.downcase == "list" || optMode.downcase == "help"
    puts "optMode Choices:
* debug - Produce a debug build
* 3     - Use -O3 and include debug info
* 2     - Use -O2 and include debug info
* 1     - Use -O1 and include debug info
* fast - Produce an optimized build"
    exit 0
  else
    puts "Unknown optMode: #{optMode}, use 'list' to see options"
    exit 1
  end

  # Some downstream projects might use these, so let's enable them here to be
  # strict.
  cmdLine += " -Dstrict_multi_assign -Dno_number_autocast"

  cmdLine += " " + t.prerequisites[-1]

  sh cmdLine

  if optMode == OPT_MODE_FAST
    sh "strip --strip-unneeded #{t.name}"
  end
end

# Generate rules for all example binaries.
#
# This assumes that for each binary 'bin/foo', there is a corresponding
# 'examples/foo.cr' source file.
EXAMPLE_BINARIES.each do |bin|
  srcfile = "#{bin.gsub("bin/", "examples/")}.cr"
  file bin, [:optMode] => EXAMPLE_DEPS + [srcfile] do |t, args|
    args.with_defaults(:optMode => OPT_MODE_DEBUG)
    buildExample(t, args, args[:optMode])
  end
end

directory DOCS_DIR => [SHARD_LOCK, LIB_DIR] + LIB_SOURCES do
  sh %|crystal docs --project-name=RemiAudio --source-url-pattern='https://chiselapp.com/user/MistressRemilia/repository/remiaudio/file?name=%{path}&ci=%{refname}&ln=%{line}' --source-refname=tip|
end
CLOBBER << DOCS_DIR

################################################################################

multitask :default => [DOCS_DIR, :examples]

desc "Update Shards"
task :deps => [SHARD_LOCK, LIB_DIR]

desc "Build example Au player"
task :auplayer, [:optMode] => [AU_PLAYER_BIN]

desc "Build example TCP Au player"
task :tcpauplayer, [:optMode] => [TCP_AU_PLAYER_BIN]

desc "Build example CUE sheet player"
task :cueplayer, [:optMode] => [CUE_PLAYER_BIN]

desc "Build example Au -> WAV converter"
task :au2wav, [:optMode] => [AU_TO_WAV_BIN]

desc "Build example WAV -> Au converter"
task :wav2au, [:optMode] => [WAV_TO_AU_BIN]

desc "Build example Biquad Filter Plotter"
task :biquadplot, [:optMode] => [BIQUADPLOT_BIN]

desc "Build example Audio Stats tool"
task :audiostats, [:optMode] => [AUDIO_STATS_BIN]

desc "Build example sine generator (Au) example"
task :gensineau, [:optMode] => [GEN_SINE_AU_BIN]

desc "Build resample example"
task :resample, [:optMode] => [RESAMPLE_BIN]

desc "Build all examples"
task :examples, [:optMode] => EXAMPLE_BINARIES

desc "Run unit tests"
task :test => [:deps] + LIB_SOURCES + SPEC_SOURCES do |_|
  sh "crystal spec -O2 --debug -p"
end

desc "Rebuild documentation"
task :docs do |_|
  sh "rm -rf #{DOCS_DIR}"
  Rake::Task[DOCS_DIR].execute
end

desc "Run Ameba"
task :lint do |_|
  sh "ameba"
end
