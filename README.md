# RemiAudio

RemiAudio is a collection of audio format I/O routines, codecs, and
processors/effects written entirely in [Crystal](https://crystal-lang.org/).

Wanna support Remilia? [Buy me a coffee on Ko-Fi](https://ko-fi.com/L4L614QNC),
or support me through Liberapay.

<a href='https://ko-fi.com/L4L614QNC' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>
<a href="https://liberapay.com/RemiliaScarlet/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

## Features

A list of supported features and planned features.

* __WAV format support__
  * [x] Reading of WAV files
  * [x] Writing of WAV files
  * [x] 1 to 24 channels
  * [x] Sample rates from 8000hz to 352800hz
  * [x] Integer sample formats: unsigned 8-bit, signed 12, 16, 24, 32, and 64-bit
  * [x] IEEE Floating Point sample formats: 32-bit and 64-bit
  * [x] μ-law and A-law sample formats
  * [ ] Extensible formats (ADPCM, MP3, etc.)
  * [ ] Related RIFF chunks (cue , inst, smpl, ltxt, note, labl, plst, etc.)
* __Au format support__
  * [x] Reading of [Au](https://en.wikipedia.org/wiki/Au_file_format) files
  * [x] Writing of AU files
  * [x] 1 or 2 channels
  * [x] Integer sample formats: unsigned 8-bit, signed 16, 24, and 32-bit
  * [x] IEEE Floating Point sample formats: 32-bit and 64-bit
  * [x] Sample rates from 8000hz to 352800hz
* __Encoders/Decoders__
  - [x] YM2610 ADPCM-A codec
  - [ ] YM2610 ADPCM-B codec
  - [x] High quality μ-law and A-law codec
* __Processing__
  - [x] Bit-depth conversion with optional TDPF dithering
  - [x] Sample format conversions
  - [x] High-quality sample rate conversions
* __Filters__
  - [x] Multi-mode biquad Filter (lowpass, highpass, bandpass, notch, allpass, peaking EQ, lowshelf, highshelf)
  - [x] Separate biquad filter optimized for lowpass usage
  - [x] Lowpass filter similar to what's found in a CEM3394
  - [x] Lowpass filter similar to a SSM2040
  - [x] "Hornet Filter", a fun approximation of an EDP Wasp's lowpass filter
  - [x] Lowpass filter similar to what's found on a Korg MS-20
* __Effects__
  - [x] Soft clipping
  - [x] Stereo enhancement
  - [x] MVerb reverb effect
  - [x] Zita-Rev1-based reverb effect
  - [x] Freeverb-based reverb effect (`Schroeder` class)
  - [x] Chorus effect inspired by an 80s synth with model number ending in "-60"
  - [x] Parametric EQ, mono and stereo variations, with an arbitrary number of bands
* __Demuxing__
  - [x] Basic Ogg demuxer
* __Audio Backend Abstraction__
  - [x] PulseAudio
  - [x] PortAudio
  - [x] libao
* __Other__
  - [x] Windowing functions (Blackman, Blackman-Harris, Hamming, Nuttall, Blackman-Nuttal, Hann)
  - [x] CUE sheet reading and writing
  - [x] XSPF/JSPF playlist reading and writing
  - [x] Vorbis Comments handling (read-only)

## How do I get set up?

Add this to your application's shard.yml:

```yaml
dependencies:
  remiaudio:
    fossil: https://chiselapp.com/user/MistressRemilia/repository/remiaudio
```

You will need [Fossil](https://fossil-scm.org/) installed to clone this repo and
the dependency.  You will also need Shards v0.17.1 or later.  If you have an
earlier version of Shards, you will need to to build the [latest
version](https://github.com/crystal-lang/shards/) manually.

## Usage

API documentation can be found here:
[https://remilia.sdf.org/programming/docs/remiaudio](https://remilia.sdf.org/programming/docs/remiaudio)

## Version Control

RemiAudio sources are managed using [Fossil](https://www.fossil-scm.org/), a
distributed version control system. The [Fossil
repository](https://chiselapp.com/user/MistressRemilia/repository/remiaudio/)
contains the urtext, tickets, and wiki.

If you are reading this on GitLab or some other Git repository or service, then
you are looking at a mirror. The names of check-ins and other artifacts in a Git
mirror are different from the official names for those objects. The official
names for check-ins are found in a footer on the check-in comment for authorized
mirrors. The official check-in name can also be seen in the manifest.uuid file
in the root of the tree. Always use the official name, not the Git-name, when
communicating about a RemiAudio check-in.

## Development

### Help Wanted/Known Issues

* The MP3 decoder is not perfect.  Some files play fine, others have minor
  artifacts, and some just crash it.
* The filters seem to be stable and they already sound pretty good, but I am no
  DSP expert by any means, so there is likely room for improvement.

### Style info

I use a somewhat non-standard style for my code.

- Keep lines 118 characters or shorter.  Obviously sometimes you can't, but
  please try.  Use 80 or 115 characters for Markdown files, though.
- Please use pascalCase for variable and method names.  Use CamelCase for type
  names.  Use UPPER_SNAKE_CASE for constants.
- ALWAYS put parentheses around method parameters, except for these methods:
  `puts`, `pp`, `p`, `raise`, `sleep`, `spawn`, `loop`, and `exit`.
- Always the full `do |foo|...end` syntax with blocks, except when it's all on
  one line, then always use { and }.
- The type name for exceptions end with Error.  For example, `ExternalProgramError`.

### How do I contribute?

1. Go to [https://chiselapp.com/user/MistressRemilia/repository/remiaudio/](https://chiselapp.com/user/MistressRemilia/repository/remiaudio/)
   and clone the Fossil repository.
2. Create a new branch for your feature.
3. Push locally to the new branch.
4. Create a [bundle](https://fossil-scm.org/home/help?cmd=bundle) with Fossil
   that contains your changes.
5. Get in contact with me.

## Contributors

* Remilia Scarlet - creator and maintainer
  * Homepage: [https://remilia.sdf.org/](https://remilia.sdf.org/)
  * Fediverse: [@remilia@nanako-さめ.mooo.com](https://nanako-さめ.mooo.com/@remilia)
  * Email: zremiliaz@postzeoz.jpz  My real address does not contain Z's

## Links and Licenses

* [MVerb](https://github.com/martineastwood/mverb) by Martin Eastwood (GPLv3 License)
* [Zita-Rev1](https://kokkinizita.linuxaudio.org/linuxaudio/zita-rev1-doc/quickguide.html)
  by Fons Adriaensen (GPLv3 License)
* [YK Chorus](https://github.com/SpotlightKid/ykchorus/) by Spotlightkid (GPLv2 License)
* [libsamplerate](https://libsndfile.github.io/libsamplerate/index.html) by Erik de Castro Lopo (2-clause BSD License)

RemiAudio itself is mostly under the [GNU Affero General Public
License](https://www.gnu.org/licenses/agpl-3.0.html) version 3, except for the
items listed above.
