#!/bin/bash

set -e

if [[ -z "$1" ]]; then
    echo "Usage: `basename "$0"` <version without v>"
    exit 1
fi

fossil tarball v$1 remiaudio-$1.tar.gz --name remiaudio-$1
gunzip remiaudio-$1.tar.gz
bzip2 -9 remiaudio-$1.tar
tar tvjf remiaudio-$1.tar.bz2

echo "SHA256: $(sha256sum remiaudio-$1.tar.bz2 | cut -d ' ' -f 1)"
echo "MD5: $(md5sum remiaudio-$1.tar.bz2 | cut -d ' ' -f 1)"
echo "Size: $(stat -c '%s' remiaudio-$1.tar.bz2) bytes"
echo "Created remiaudio-$1.tar.bz2"
