#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "json"
require "uri"
require "xml"

module RemiAudio::Xspf
  module XmlDateConverter
    def self.from_json(pull : JSON::PullParser) : Time
      Time.parse_rfc3339(pull.read_string)
    end

    def self.to_json(value : Time, json : JSON::Builder) : Nil
      value.to_rfc3339.to_json(json)
    end
  end

  module AttributionJsonConverter
    def self.from_json(pull : JSON::PullParser) : Attribution?
      ret : Attribution? = nil
      pull.read_array do
        ret = Attribution.new unless ret
        pull.read_begin_object
        case pull.read_object_key
        when "identifier" then ret.identifiers << URI.parse(pull.read_string)
        when "location" then ret.locations << URI.parse(pull.read_string)
        else raise Error.new("Bad JSON, 'attribution' element has unexpected data at #{pull.line_number}")
        end
        pull.read_end_object
      end
      ret
    end

    def self.to_json(value : Attribution, json : JSON::Builder) : Nil
      json.array do
        value.locations.each do |loc|
          json.object { json.field("location", loc.to_s) }
        end

        value.identifiers.each do |ident|
          json.object { json.field("identifier", ident.to_s) }
        end
      end
    end
  end

  module LinksJsonConverter
    def self.from_json(pull : JSON::PullParser) : Array(Link)
      ret = [] of Link
      pull.read_array do
        pull.read_begin_object
        rel = pull.read_object_key
        lnk = pull.read_string
        ret << Link.new(URI.parse(rel), URI.parse(lnk))
        pull.read_end_object
      end
      ret
    end

    def self.to_json(value : Array(Link), json : JSON::Builder) : Nil
      json.array do
        value.each do |lnk|
          json.object { json.field(lnk.rel.to_s, lnk.content.to_s) }
        end
      end
    end
  end

  module MetasJsonConverter
    def self.from_json(pull : JSON::PullParser) : Array(Meta)
      ret = [] of Meta
      pull.read_array do
        pull.read_begin_object
        rel = pull.read_object_key
        meta = pull.read_string
        ret << Meta.new(URI.parse(rel), meta)
        pull.read_end_object
      end
      ret
    end

    def self.to_json(value : Array(Meta), json : JSON::Builder) : Nil
      json.array do
        value.each do |meta|
          json.object { json.field(meta.rel.to_s, meta.content.to_s) }
        end
      end
    end
  end
end
