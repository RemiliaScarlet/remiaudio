#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

class ::URI
  ###
  ### URIs are treated simply as a scalar in JSOn, parsed with URI.parse at load
  ### time.
  ###

  def to_json(json : JSON::Builder) : Nil
    json.scalar(self.to_s)
  end

  def self.new(json : JSON::PullParser)
    self.parse(json.read_string)
  end
end
