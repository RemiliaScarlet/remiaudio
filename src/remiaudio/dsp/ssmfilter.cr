#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./lpfilter"

###
### Some calculations taken from this code, by Joep Vanlier:
### https://github.com/JoepVanlier/JSFX/blob/master/Filther/Filther.jsfx
###
### Compile with -Dhqssmfilter if you want to use the internal Math.tanh at the
### cost of speed.  Compile without that flag to use RemiMath.fastTanh for
### better speed at the cost of a small amount of accuracy.
###

module RemiAudio::DSP
  # Implements a lowpass filter that is similar to a SSM2040.
  class SsmFilter
    include ::RemiAudio::DSP::LPFilter

    @d0 : Float64 = 0.0
    @d1 : Float64 = 0.0
    @d2 : Float64 = 0.0
    @d3 : Float64 = 0.0
    @di : Float64 = 0.0
    @u0 : Float64 = 0.0
    @u1 : Float64 = 0.0
    @u2 : Float64 = 0.0
    @u3 : Float64 = 0.0

    @k2 : Float64 = 0.0
    @hhitau : Float64 = 0.0

    def initialize(sampleRate)
      @sampleRate = sampleRate.to_f64!
      @invSampleRate = 1.0 / @sampleRate
      updateCoefficients
    end

    @[AlwaysInline]
    def updateCoefficients : Nil
      @active = @cutoff <= ENABLE_AT
      h = 1.0 / @sampleRate
      omega = RemiMath::TWO_PI * @cutoff * @invSampleRate * 0.5
      f = 20 * @sampleRate * Math.tan(omega * 0.5)
      @k2 = 4.6 * @resonance
      itau = 0.00192 * 50.0 * f
      @hhitau = 0.5 * h * itau
    end

    # Clears the internal buffers.
    @[AlwaysInline]
    def reset : Nil
      @d0 = 0.0
      @d1 = 0.0
      @d2 = 0.0
      @d3 = 0.0
      @di = 0.0
      @u0 = 0.0
      @u1 = 0.0
      @u2 = 0.0
      @u3 = 0.0
    end

    # Sets the cutoff frequency of the filter.  If this is less than the NyQuist
    # limit, then the filter will be disabled.
    @[AlwaysInline]
    def cutoff=(val : Float64)
      return if val == @cutoff
      @cutoff = val.clamp(CUTOFF_MIN, CUTOFF_MAX)
      updateCoefficients
    end

    # Sets the resonance of the filter.  This will be clamped to
    # `RESONANCE_MIN..RESONANCE_MAX`.
    @[AlwaysInline]
    def resonance=(val : Float64)
      return if val == @resonance
      @resonance = val.clamp(RESONANCE_MIN, RESONANCE_MAX)
      updateCoefficients
    end

    # Sets both the cutoff frequency and resonance of the filter.  If the cutoff
    # is less than the NyQuist limit, then the filter will be disabled.  The
    # cutoff will be clamped to `RESONANCE_MIN..RESONANCE_MAX`.
    @[AlwaysInline]
    def set(newCutoff : Float64, newResonance : Float64)
      return if @cutoff == newCutoff && @resonance == newResonance
      @cutoff = newCutoff.clamp(CUTOFF_MIN, CUTOFF_MAX)
      @resonance = newResonance.clamp(RESONANCE_MIN, RESONANCE_MAX)
      updateCoefficients
    end

    @[AlwaysInline]
    private def myTanh(val : Float64) : Float64
      {% begin %}
        {% if flag?(:hqssmfilter) %}
          Math.tanh(val)
        {% else %}
          RemiMath.fastTanh(val)
        {% end %}
      {% end %}
    end

    def process(sample : Float64) : Float64
      return sample unless @active
      distd0d1   = myTanh(@d0 + @d1)
      distd1d2   = myTanh(@d1 + @d2)
      distd2d3   = myTanh(@d2 + @d3)
      distd0difb = myTanh(@d0 + @di - myTanh(@k2 * @d3))

      3.times do |_|
        distu0u1   = myTanh(@u0 + @u1)
        distu1u2   = myTanh(@u1 + @u2)
        distu2u3   = myTanh(@u2 + @u3)
        fbterm     = myTanh(@k2 * @u3)
        distu0uifb = myTanh(@u0 + sample - fbterm)

        ddistu0uifb = 1 - distu0uifb * distu0uifb

        f0 = -@d0 + @u0 + @hhitau * (distd0difb + distu0uifb)
        f1 = -@d1 + @u1 + @hhitau * (distd0d1   + distu0u1)
        f2 = -@d2 + @u2 + @hhitau * (distd1d2   + distu1u2)
        f3 = -@d3 + @u3 + @hhitau * (distd2d3   + distu2u3)

        a = @hhitau * ddistu0uifb + 1
        b = -@k2 * @hhitau * (1 - fbterm     * fbterm) * ddistu0uifb
        c = @hhitau * (1 - distu0u1   * distu0u1)
        d = c + 1
        e = @hhitau * (1 - distu1u2   * distu1u2)
        f = e + 1
        g = @hhitau * (1 - distu2u3   * distu2u3)
        h = g + 1

        idet = 1.0 / ((a * d * f * h) - (b * c * e * g))
        @u0 = @u0 - ((-b * d * f * f3) + (b * d * f2 * g) - (b * e * f1 * g) + (d * f * f0 * h)) * idet
        @u1 = @u1 - ((a * f * f1 * h)  + (b * c * f * f3) - (b * c * f2 * g) - (c * f * f0 * h)) * idet
        @u2 = @u2 - ((a * d * f2 * h)  - (a * e * f1 * h) - (b * c * e * f3) + (c * e * f0 * h)) * idet
        @u3 = @u3 - ((a * d * f * f3)  - (a * d * f2 * g) + (a * e * f1 * g) - (c * e * f0 * g)) * idet
      end

      @d0 = @u0
      @d1 = @u1
      @d2 = @u2
      @d3 = @u3
      @di = sample

      @u3
    end
  end
end
