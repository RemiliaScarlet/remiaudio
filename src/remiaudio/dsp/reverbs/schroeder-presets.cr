#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####   Based on MVerb
####   Copyright (c) 2010 Martin Eastwood
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./reverb"

module RemiAudio::DSP
  class Schroeder < Reverb
    # Defines a set of parameters for `Schroeder`.
    class Preset < Reverb::Preset
      getter roomSize : Float64 = DEFAULT_ROOM
      getter damping : Float64 = DEFAULT_DAMPING
      getter width : Float64 = DEFAULT_WIDTH
      getter wet : Float64 = DEFAULT_WET

      # Creates a new `Preset` instance.
      def initialize
      end

      # :ditto:
      def initialize(*, @roomSize : Float64 = DEFAULT_ROOM,
                     @damping : Float64 = DEFAULT_DAMPING,
                     @width : Float64 = DEFAULT_WIDTH,
                     @wet : Float64 = DEFAULT_WET)
        # Do a bunch of checks
        Schroeder.checkRoomSize(@roomSize)
        Schroeder.checkDamping(@damping)
        Schroeder.checkWidth(@width)
        Schroeder.checkWet(@wet)
      end

      def roomSize=(value : Float64)
        Schroeder.checkRoomSize(value)
        @roomSize = value
      end

      def damping=(value : Float64)
        Schroeder.checkDamping(value)
        @damping = value
      end

      def width=(value : Float64)
        Schroeder.checkWidth(value)
        @width = value
      end

      def wet=(value : Float64)
        Schroeder.checkWet(value)
        @wet = value
      end
    end

    # Names for the pre-constructed `Preset` instances in `MVerb::PRESETS`.
    enum PresetNames
      # A good default for General MIDI use.
      GmDefault

      # A small room.
      Room

      # A large concert hall.
      Hall
    end

    # Pre-constructed `Preset` instances for `MVerb` that correspond to the
    # names in `PresetNames`.
    PRESETS = {
      PresetNames::GmDefault => Preset.new(
        roomSize: 0.5,
        damping: 0.5,
        width: 1.0),

      PresetNames::Room => Preset.new(
        roomSize: 0.3,
        damping: 0.3,
        width: 0.94
      ),

      PresetNames::Hall => Preset.new(
        roomSize: 0.9,
        damping: 0.7,
        width: 1.1
      )
    }
  end
end
