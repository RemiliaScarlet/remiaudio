#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####   Based on MVerb
####   Copyright (c) 2010 Martin Eastwood
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./reverb"

module RemiAudio::DSP
  class ZitaReverb
    # Defines a set of parameters for `ZitaReverb`.
    class Preset < Reverb::Preset
      getter predelay : Float64 = DEFAULT_PRE_DELAY
      getter crossover : Float64 = DEFAULT_REVERB_XOVER
      getter timeLow : Float64 = DEFAULT_REVERB_TIME_LOW
      getter timeHigh : Float64 = DEFAULT_REVERB_TIME_HIGH
      getter damping : Float64 = DEFAULT_REVERB_DAMPING
      getter eq1Freq : Float64 = DEFAULT_REVERB_EQ1_FREQ
      getter eq1Gain : Float64 = DEFAULT_REVERB_EQ1_GAIN
      getter eq2Freq : Float64 = DEFAULT_REVERB_EQ2_FREQ
      getter eq2Gain : Float64 = DEFAULT_REVERB_EQ2_GAIN

      # Creates a new `Preset` instance.
      def initialize
      end

      # :ditto:
      def initialize(*, @predelay : Float64 = DEFAULT_PRE_DELAY,
                     @crossover : Float64 = DEFAULT_REVERB_XOVER,
                     @timeLow : Float64 = DEFAULT_REVERB_TIME_LOW,
                     @timeHigh : Float64 = DEFAULT_REVERB_TIME_HIGH,
                     @damping : Float64 = DEFAULT_REVERB_DAMPING,
                     @eq1Freq : Float64 = DEFAULT_REVERB_EQ1_FREQ,
                     @eq1Gain : Float64 = DEFAULT_REVERB_EQ1_GAIN,
                     @eq2Freq : Float64 = DEFAULT_REVERB_EQ2_FREQ,
                     @eq2Gain : Float64 = DEFAULT_REVERB_EQ2_GAIN)
        # Do a bunch of checks
        ZitaReverb.checkPredelay(@predelay)
        ZitaReverb.checkCrossover(@crossover)
        ZitaReverb.checkTimeLow(@timeLow)
        ZitaReverb.checkTimeHigh(@timeHigh)
        ZitaReverb.checkDamping(@damping)
        ZitaReverb.checkEQ1Freq(@eq1Freq)
        ZitaReverb.checkEQ1Gain(@eq1Gain)
        ZitaReverb.checkEQ2Freq(@eq2Freq)
        ZitaReverb.checkEQ2Gain(@eq2Gain)
      end

      def predelay=(value : Float64)
        ZitaReverb.heckPredelay(value)
        @predelay = value
      end

      def crossover=(value : Float64)
        ZitaReverb.checkCrossover(value)
        @crossover = value
      end

      def timeLow=(value : Float64)
        ZitaReverb.checkTimeLow(value)
        @timeLow = value
      end

      def timeHigh=(value : Float64)
        ZitaReverb.checkTimeHigh(value)
        @timeHigh = value
      end

      def damping=(value : Float64)
        ZitaReverb.checkDamping(value)
        @damping = value
      end

      def eq1Freq=(value : Float64)
        ZitaReverb.checkEQ1Freq(value)
        @eq1Freq = value
      end

      def eq1Gain=(value : Float64)
        ZitaReverb.checkEQ1Gain(value)
        @eq1Gain = value
      end

      def eq2Freq=(value : Float64)
        ZitaReverb.checkEQ2Freq(value)
        @eq2Freq = value
      end

      def eq2Gain=(value : Float64)
        ZitaReverb.checkEQ2Gain(value)
        @eq2Gain = value
      end
    end

    # Names for the pre-constructed `Preset` instances in `MVerb::PRESETS`.
    enum PresetNames
      # A good default for General MIDI use.
      GmDefault

      # Akin to being stuck inside of a small room.
      SmallRoom

      # A large concert hall.
      Hall
    end

    # Pre-constructed `Preset` instances for `MVerb` that correspond to the
    # names in `PresetNames`.
    PRESETS = {
      PresetNames::GmDefault => Preset.new(
        predelay: DEFAULT_PRE_DELAY,
        crossover: DEFAULT_REVERB_XOVER,
        timeLow: DEFAULT_REVERB_TIME_LOW,
        timeHigh: DEFAULT_REVERB_TIME_HIGH,
        damping: DEFAULT_REVERB_DAMPING,
        eq1Freq: DEFAULT_REVERB_EQ1_FREQ,
        eq1Gain: DEFAULT_REVERB_EQ1_GAIN,
        eq2Freq: DEFAULT_REVERB_EQ2_FREQ,
        eq2Gain: DEFAULT_REVERB_EQ2_GAIN),

      PresetNames::SmallRoom => Preset.new(
        predelay: 0.03,
        crossover: 300.0,
        timeLow: 1.0,
        timeHigh: 1.1,
        damping: 7500.0,
        eq1Freq: 40.0,
        eq2Freq: 160.0,
        eq1Gain: 0.0,
        eq2Gain: 0.0),

      PresetNames::Hall => Preset.new(
        predelay: 0.042,
        crossover: 290.0,
        timeLow: 3.0,
        timeHigh: 4.0,
        damping: 3900.0,
        eq1Freq: 200.0,
        eq2Freq: 8000.0,
        eq1Gain: -1.1,
        eq2Gain: 1.2)
    }
  end
end
