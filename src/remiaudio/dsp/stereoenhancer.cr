#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

module RemiAudio::DSP::StereoEnhancer
  # Applies a stereo enhancement effect to the audio blocks.  The coeff
  # parameter defines how much widening (or narrowing) to apply.  0.5 is
  # neutral, 0.0 mono-fies it, and values above 0.5 widen the sound.  The
  # expected range is 0.0 to 1.5.
  @[AlwaysInline]
  def self.process(blockLeft : Array(Float64)|Slice(Float64), blockRight : Array(Float64)|Slice(Float64),
                   coeff : Float64) : Nil
    sampleL : Float64 = 0.0
    sampleR : Float64 = 0.0
    adjust : Float64 = 0.0
    stereo : Float64 = 0.0

    blockLeft.size.times do |i|
      sampleL = blockLeft[i]
      sampleR = blockRight[i]
      adjust = (sampleL + sampleR) * 0.5
      stereo = (sampleL - sampleR) * coeff
      blockLeft[i] = adjust + stereo
      blockRight[i] = adjust - stereo
    end
  end

  # Applies a stereo enhancement effect to the audio blocks.  The coeff
  # parameter defines how much widening (or narrowing) to apply.  0.5 is
  # neutral, 0.0 mono-fies it, and values above 0.5 widen the sound.  The
  # expected range is 0.0 to 1.5.
  @[AlwaysInline]
  def self.process(blockLeft : Array(Float32)|Slice(Float32), blockRight : Array(Float32)|Slice(Float32),
                   coeff : Float64) : Nil
    sampleL : Float32 = 0.0
    sampleR : Float32 = 0.0
    adjust : Float64 = 0.0
    stereo : Float64 = 0.0

    blockLeft.size.times do |i|
      sampleL = blockLeft[i]
      sampleR = blockRight[i]
      adjust = (sampleL + sampleR) * 0.5
      stereo = (sampleL - sampleR) * coeff
      blockLeft[i] = (adjust + stereo).to_f32!
      blockRight[i] = (adjust - stereo).to_f32!
    end
  end

  # Applies a stereo enhancement effect to the interleaved audio
  # block.  The coeff parameter defines how much widening (or
  # narrowing) to apply.  0.5 is neutral, 0.0 mono-fies it, and values
  # above 0.5 widen the sound.  The expected range is 0.0 to 1.5.
  @[AlwaysInline]
  def self.process(block : Array(Float64)|Slice(Float64), coeff : Float64) : Nil
    sampleL : Float64 = 0.0
    sampleR : Float64 = 0.0
    adjust : Float64 = 0.0
    stereo : Float64 = 0.0
    pos : Int32 = 0

    while pos < block.size
      sampleL = block[pos]
      sampleR = block[pos + 1]
      adjust = (sampleL + sampleR) * 0.5
      stereo = (sampleL - sampleR) * coeff
      block[pos] = adjust + stereo
      block[pos + 1] = adjust - stereo
      pos += 2
    end
  end

  # Applies a stereo enhancement effect to the interleaved audio
  # block.  The coeff parameter defines how much widening (or
  # narrowing) to apply.  0.5 is neutral, 0.0 mono-fies it, and values
  # above 0.5 widen the sound.  The expected range is 0.0 to 1.5.
  @[AlwaysInline]
  def self.process(block : Array(Float32)|Slice(Float32), coeff : Float64) : Nil
    sampleL : Float64 = 0.0
    sampleR : Float64 = 0.0
    adjust : Float64 = 0.0
    stereo : Float64 = 0.0
    pos : Int32 = 0

    while pos < block.size
      sampleL = block[pos].to_f64!
      sampleR = block[pos + 1].to_f64!
      adjust = (sampleL + sampleR) * 0.5
      stereo = (sampleL - sampleR) * coeff
      block[pos] = (adjust + stereo).to_f32!
      block[pos + 1] = (adjust - stereo).to_f32!
      pos += 2
    end
  end
end
