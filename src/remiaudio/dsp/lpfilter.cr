#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "../common"

module RemiAudio::DSP
  module LPFilter
    # The minimum cutoff frequency.
    CUTOFF_MIN = 40.0

    # The maximum cutoff frequency.
    CUTOFF_MAX = 22000.0

    # The mininum resonance amount.
    RESONANCE_MIN = 0.0

    # The maximum resonance amount.
    RESONANCE_MAX = 1.0

    # If the cutoff is set above this, the filter is automatically disabled.
    #
    # This is above human hearing limits.
    ENABLE_AT = CUTOFF_MAX - 1.0

    # The current cutoff of the filter.
    @[AlwaysInline]
    getter cutoff : Float64 = CUTOFF_MAX

    # The current resonance of the filter as a Q value.
    @[AlwaysInline]
    getter resonance : Float64 = RESONANCE_MIN

    getter sampleRate : Float64
    getter invSampleRate : Float64

    property? active : Bool = true

    def sampleRate=(val : Int|Float) : Nil
      @sampleRate = val.to_f64!
      @invSampleRate = 1.0 / @sampleRate
      updateCoefficients
    end

    @[AlwaysInline]
    def process(sample : Float32) : Float32
      self.process(sample.to_f64!).to_f32!
    end

    # Converts a MIDI value (0-127) to a cutoff frequency in hertz.
    @[AlwaysInline]
    def self.midiToCutoff(val : UInt8) : Float64
      (val * (CUTOFF_MAX - CUTOFF_MIN)) / 127.0 + CUTOFF_MIN
    end

    macro included
      {% begin %}
        {% for klass in [:Array, :Slice] %}
          {% for typ in [:Float32, :Float64] %}
            # "Runs" the filter over `block`.
            def process(block : {{klass.id}}({{typ.id}})) : {{@type}}
              block.size.times do |t|
                block[t] = process(block[t])
              end
              self
            end
          {% end %}
        {% end %}
      {% end %}
    end

    abstract def reset : Nil
    abstract def cutoff=(val : Float64)
    abstract def resonance=(val : Float64)
    abstract def set(newCutoff : Float64, newResonance : Float64)
    abstract def process(sample : Float64) : Float64
    abstract def updateCoefficients : Nil
  end
end
