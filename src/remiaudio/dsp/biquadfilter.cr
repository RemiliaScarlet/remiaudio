#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

###
### https://www.musicdsp.org/en/latest/Filters/197-rbj-audio-eq-cookbook.html
### plus some referencing of the SoX source code (biquads.c)
###

module RemiAudio::DSP
  # Implements a digital biquad filter.
  class BiQuadFilter
    enum FilterMode
      LowPass
      HighPass
      BandPass
      Notch
      AllPass
      PeakingEQ
      LowShelf
      HighShelf
    end

    # The minimum resonance value.
    RESONANCE_MIN = Float64::MIN_POSITIVE

    # The maximum resonance value.
    RESONANCE_MAX = 1.0

    # Whether or not the filter is currently active.
    property? active : Bool = false

    getter b0 : Float64 = 0.0f64
    getter b1 : Float64 = 0.0f64
    getter b2 : Float64 = 0.0f64
    getter a1 : Float64 = 0.0f64
    getter a2 : Float64 = 0.0f64

    # The current frequency of the filter.
    @[AlwaysInline]
    getter freq : Float64 = 20000.0

    # The current gain of the filter.
    #
    # Setting the filter to be a lowpass, highpass, bandpass, notch, or allpass
    # filter will cause this to always return 1.0.  Otherwise this returns the
    # actual gain.
    @[AlwaysInline]
    getter gain : Float64 = 0.0

    # The current width of the filter.  This may be the bandwidth, or the Q
    # value, depending on the mode the filter is set to.
    @[AlwaysInline]
    getter width : Float64 = 1.0

    # The current mode of the filter.
    @[AlwaysInline]
    getter mode : FilterMode = FilterMode::PeakingEQ

    @x1 : Float64 = 0.0f64
    @x2 : Float64 = 0.0f64
    @y1 : Float64 = 0.0f64
    @y2 : Float64 = 0.0f64

    # This internal value is a linear value, not decibels.
    @postGain : Float64 = 1.0

    @sampleRate : Float64
    @invSampleRate : Float64

    # Creates a new `BiQuadFilter`.
    def initialize(newSampleRate)
      @sampleRate = newSampleRate.to_f64!
      @invSampleRate = 1.0 / @sampleRate
    end

    def sampleRate=(val : Int|Float) : Nil
      @sampleRate = val.to_f64!
      @invSampleRate = 1.0 / @sampleRate
    end

    # The amount of gain to apply after the EQ, in decibels.
    def postGain : Float64
      RemiAudio.linearToDecibels(@postGain)
    end

    # The amount of gain to apply after the EQ, in decibels.
    def postGain=(value : Float64) : Nil
      @postGain = RemiAudio.decibelsToLinear(value)
    end

    # Clears the internal buffer
    @[AlwaysInline]
    def clearBuffer : Nil
      @x1 = 0.0f64
      @x2 = 0.0f64
      @y1 = 0.0f64
      @y2 = 0.0f64
    end

    def deactivate : Nil
      @active = false
    end

    def activate : Nil
      @active = true
    end

    # Clears the internal buffer.  This does not change the settings of the EQ.
    @[AlwaysInline]
    def reset
      @x1 = 0.0f64
      @x2 = 0.0f64
      @y1 = 0.0f64
      @y2 = 0.0f64
    end

    # Sets the coefficients for the filter.
    @[AlwaysInline]
    protected def setCoefficients(na0 : Float64, na1 : Float64, na2 : Float64,
                                  nb0 : Float64, nb1 : Float64, nb2 : Float64)
      @b0 = nb0 / na0
      @b1 = nb1 / na0
      @b2 = nb2 / na0
      @a1 = na1 / na0
      @a2 = na2 / na0
    end

    @[AlwaysInline]
    protected def calcAlphaQ(omega : Float64, val : Float64) : Float64
      Math.sin(omega) / (2 * val)
    end

    @[AlwaysInline]
    protected def calcAlphaBandwidth(omega : Float64, val : Float64) : Float64
      sn = Math.sin(omega)
      sn * Math.sinh((Math::LOG2 / 2) * val * (omega / sn))
    end

    @[AlwaysInline]
    protected def calcAlphaShelf(omega : Float64, bigA : Float64, val : Float64) : Float64
      sn = Math.sin(omega)
      (sn / 2) * Math.sqrt((bigA + 1 / bigA) * (1 / val - 1) + 2)
    end

    @[AlwaysInline]
    protected def calcBigA(gain : Float64) : Float64
      Math.exp(gain / 20 * Math.log(10.0))
    end

    @[AlwaysInline]
    protected def calcBigAAlt(gain : Float64) : Float64
      Math.exp(gain / 40 * Math.log(10.0))
    end

    @[AlwaysInline]
    protected def calcOmega(cutoff : Float64) : Float64
      RemiMath::TWO_PI * cutoff * @invSampleRate
    end

    # Puts the filter into lowpass mode, setting the cutoff frequency and the
    # resonance ("Q" value).  The resonance will be clamped to
    # `RESONANCE_MIN..RESONANCE_MAX`.
    #
    # This deactivates the filter if cutoff is greater than the NyQuist limit.
    # If `resonance` is less than or equal to 0, then resonance is set to
    # `RESONANCE_MIN`.
    def setLowPass(cutoff : Float64, resonance : Float64) : Nil
      resonance = RemiAudio.decibelsToLinear(resonance.clamp(RESONANCE_MIN, RESONANCE_MAX))
      @freq = cutoff
      @width = resonance
      @gain = 1.0
      @mode = FilterMode::LowPass
      omega : Float64 = calcOmega(cutoff)

      if omega <= Math::PI
        @active = true
        alpha : Float64 = calcAlphaQ(omega, resonance)
        cosO  : Float64 = Math.cos(omega)
        b0    : Float64 = (1.0f64 - cosO) * 0.5f64
        b1    : Float64 = 1.0f64 - cosO
        b2    : Float64 = (1.0f64 - cosO) * 0.5f64
        a0    : Float64 = 1.0f64 + alpha
        a1    : Float64 = -2.0f64 * cosO
        a2    : Float64 = 1.0f64 - alpha
        setCoefficients(a0, a1, a2, b0, b1, b2)
      else
        @active = false
      end
    end

    # Puts the filter into highpass mode, setting the cutoff frequency and the
    # resonance ("Q" value).
    #
    # This deactivates the filter if cutoff is greater than the NyQuist limit.
    # If `resonance` is less than or equal to 0, then resonance is set to
    # `RESONANCE_MIN`.
    def setHighPass(cutoff : Float64, resonance : Float64) : Nil
      resonance = Float64::MIN_POSITIVE if resonance <= 0
      @freq = cutoff
      @width = resonance
      @gain = 1.0
      @mode = FilterMode::HighPass
      omega : Float64 = calcOmega(cutoff)

      if omega <= Math::PI
        @active = true
        alpha : Float64 = calcAlphaQ(omega, resonance)
        cosO  : Float64 = Math.cos(omega)
        b0    : Float64 = (1.0f64 + cosO) * 0.5f64
        b1    : Float64 = -(1.0f64 + cosO)
        b2    : Float64 = (1.0f64 + cosO) * 0.5f64
        a0    : Float64 = 1.0f64 + alpha
        a1    : Float64 = -2.0f64 * cosO
        a2    : Float64 = 1.0f64 - alpha
        setCoefficients(a0, a1, a2, b0, b1, b2)
      else
        @active = false
      end
    end

    # Puts the filter into bandpass mode, setting the center frequency and the
    # width of the filter in octaves.
    #
    # This deactivates the filter if cutoff is greater than the NyQuist limit,
    # or bandwidth is <= 0.
    def setBandPass(centerFreq : Float64, bandwidth : Float64) : Nil
      @freq = centerFreq
      @width = bandwidth
      @gain = 1.0
      @mode = FilterMode::BandPass
      omega : Float64 = calcOmega(centerFreq)

      if omega <= Math::PI && bandwidth > 0
        @active = true
        alpha : Float64 = calcAlphaBandwidth(omega, bandwidth)
        cosO  : Float64 = Math.cos(omega)
        b0    : Float64 = alpha
        b1    : Float64 = 0.0
        b2    : Float64 = -alpha
        a0    : Float64 = 1.0f64 + alpha
        a1    : Float64 = -2.0f64 * cosO
        a2    : Float64 = 1.0f64 - alpha
        setCoefficients(a0, a1, a2, b0, b1, b2)
      else
        @active = false
      end
    end

    # Puts the filter into notch (bandstop) mode, setting the center frequency
    # and the resonance ("Q" value).
    #
    # This deactivates the filter if cutoff is greater than the NyQuist limit,
    # or bandwidth is <= 0.
    def setNotch(centerFreq : Float64, bandwidth : Float64) : Nil
      @freq = centerFreq
      @width = bandwidth
      @gain = 1.0
      @mode = FilterMode::Notch
      omega : Float64 = calcOmega(centerFreq)

      if omega <= Math::PI && bandwidth > 0
        @active = true
        alpha : Float64 = calcAlphaBandwidth(omega, bandwidth)
        cosO  : Float64 = Math.cos(omega)
        b0    : Float64 = 1.0
        b1    : Float64 = -2.0 * cosO
        b2    : Float64 = 1.0
        a0    : Float64 = 1.0f64 + alpha
        a1    : Float64 = -2.0f64 * cosO
        a2    : Float64 = 1.0f64 - alpha
        setCoefficients(a0, a1, a2, b0, b1, b2)
      else
        @active = false
      end
    end

    # Puts the filter into allpass mode, setting the center frequency and the
    # resonance ("Q" value).
    #
    # This deactivates the filter if cutoff is greater than the NyQuist limit,
    # or bandwidth is <= 0.
    def setAllPass(centerFreq : Float64, bandwidth : Float64) : Nil
      @freq = centerFreq
      @width = bandwidth
      @gain = 1.0
      @mode = FilterMode::AllPass
      omega : Float64 = calcOmega(centerFreq)

      if omega <= Math::PI && bandwidth > 0
        @active = true
        alpha : Float64 = calcAlphaBandwidth(omega, bandwidth)
        cosO  : Float64 = Math.cos(omega)
        b0    : Float64 = 1.0 - alpha
        b1    : Float64 = -2.0 * cosO
        b2    : Float64 = 1.0 + alpha
        a0    : Float64 = 1.0 + alpha
        a1    : Float64 = -2.0 * cosO
        a2    : Float64 = 1.0 - alpha
        setCoefficients(a0, a1, a2, b0, b1, b2)
      else
        @active = false
      end
    end

    # Puts the filter into peaking EQ mode, setting the center frequency and the
    # resonance ("Q" value).
    #
    # This deactivates the filter if cutoff is greater than the NyQuist limit,
    # if gain is 0, or if bandwidth is <= 0.
    def setPeakingEQ(centerFreq : Float64, gain : Float64, bandwidth : Float64) : Nil
      @freq = centerFreq
      @width = bandwidth
      @gain = gain
      @mode = FilterMode::PeakingEQ
      omega : Float64 = calcOmega(centerFreq)
      bigA  : Float64 = calcBigAAlt(gain)

      if omega <= Math::PI && bigA != 0 && bandwidth > 0 && gain != 0
        @active = true
        alpha : Float64 = calcAlphaBandwidth(omega, bandwidth)
        cosO  : Float64 = Math.cos(omega)
        b0    : Float64 = 1.0 + (alpha * bigA)
        b1    : Float64 = -2.0 * cosO
        b2    : Float64 = 1.0 - (alpha * bigA)
        a0    : Float64 = 1.0 + (alpha / bigA)
        a1    : Float64 = -2.0 * cosO
        a2    : Float64 = 1.0 - (alpha / bigA)
        setCoefficients(a0, a1, a2, b0, b1, b2)
      else
        @active = false
      end
    end

    # Puts the filter into low shelf mode, setting the cutoff frequency and the
    # resonance ("Q" value).
    #
    # This deactivates the filter if cutoff is greater than the NyQuist limit,
    # if gain is 0, or if bandwidth is <= 0.
    def setLowShelf(cutoff : Float64, gain : Float64, bandwidth : Float64) : Nil
      @freq = cutoff
      @width = bandwidth
      @gain = gain
      @mode = FilterMode::LowShelf
      omega : Float64 = calcOmega(cutoff)
      bigA  : Float64 = calcBigAAlt(gain)

      if omega <= Math::PI && bigA != 0 && bandwidth > 0 && gain != 0
        @active = true
        alpha : Float64 = calcAlphaShelf(omega, bigA, bandwidth)
        cosO  : Float64 = Math.cos(omega)
        asqrt : Float64 = Math.sqrt(bigA)
        b0    : Float64 =        bigA * ((bigA + 1.0) -
                                         ((bigA - 1.0) * cosO) +
                                         (2.0 * asqrt * alpha))
        b1    : Float64 =  2.0 * bigA * ((bigA - 1.0) -
                                         ((bigA + 1.0) * cosO))
        b2    : Float64 =        bigA * ((bigA + 1.0) -
                                         ((bigA - 1.0) * cosO) -
                                         (2.0 * asqrt * alpha))
        a0    : Float64 =                (bigA + 1.0) +
                                         ((bigA - 1.0) * cosO) +
                                         (2.0 * asqrt * alpha)
        a1    : Float64 =        -2.0 * ((bigA - 1.0) +
                                         ((bigA + 1.0) * cosO))
        a2    : Float64 =                (bigA + 1.0) +
                                         ((bigA - 1.0) * cosO) -
                                         (2.0 * asqrt * alpha)
        setCoefficients(a0, a1, a2, b0, b1, b2)
      else
        @active = false
      end
    end

    # Puts the filter into low shelf mode, setting the cutoff frequency and the
    # resonance ("Q" value).
    #
    # This deactivates the filter if cutoff is greater than the NyQuist limit,
    # if gain is 0, or if bandwidth is <= 0.
    def setHighShelf(cutoff : Float64, gain : Float64, bandwidth : Float64) : Nil
      @freq = cutoff
      @width = bandwidth
      @gain = gain
      @mode = FilterMode::HighShelf
      omega : Float64 = calcOmega(cutoff)
      bigA  : Float64 = calcBigAAlt(gain)

      if omega <= Math::PI && bigA != 0 && bandwidth > 0 && gain != 0
        @active = true
        alpha : Float64 = calcAlphaShelf(omega, bigA, bandwidth)
        cosO  : Float64 = Math.cos(omega)
        asqrt : Float64 = Math.sqrt(bigA)
        b0    : Float64 =        bigA * ((bigA + 1.0) + ((bigA - 1.0) * cosO) + (2.0 * asqrt * alpha))
        b1    : Float64 = -2.0 * bigA * ((bigA - 1.0) + ((bigA + 1.0) * cosO))
        b2    : Float64 =        bigA * ((bigA + 1.0) + ((bigA - 1.0) * cosO) - (2.0 * asqrt * alpha))

        a0    : Float64 =                (bigA + 1.0) - ((bigA - 1.0) * cosO) + (2.0 * asqrt * alpha)

        a1    : Float64 =         2.0 * ((bigA - 1.0) - ((bigA + 1.0) * cosO))

        a2    : Float64 =                (bigA + 1.0) - ((bigA - 1.0) * cosO) - (2.0 * asqrt * alpha)

        setCoefficients(a0, a1, a2, b0, b1, b2)
      else
        @active = false
      end
    end

    @[AlwaysInline]
    protected def process!(sample : Float64) : Float64
      output : Float64 = ((@b0 * sample) + (@b1 * @x1) + (@b2 * @x2)) - (@a1 * @y1) - (@a2 * @y2)
      output *= @postGain
      @x2 = @x1
      @x1 = sample
      @y2 = @y1
      @y1 = output
    end

    @[AlwaysInline]
    protected def process!(sample : Float32) : Float32
      self.process!(sample.to_f64!).to_f32!
    end

    {% begin %}
      {% for klass in [:Array, :Slice] %}
        {% for typ in [:Float32, :Float64] %}
          # "Runs" the filter over `block`.  This always assumes the filter is active.
          protected def process!(block : {{klass.id}}({{typ.id}})) : BiQuadFilter
            # We can use the unsafe_ methods here since we are working on block.size.
            block.size.times do |t|
              block.unsafe_put(t, process!(block.unsafe_fetch(t)))
            end
            self
          end
        {% end %}
      {% end %}
    {% end %}

    # Processes a single sample with the filter, returning a new sample.
    @[AlwaysInline]
    def process(sample : Float64) : Float64
      if @active
        process!(sample)
      else
        @x2 = @x1
        @x1 = sample
        @y2 = @x2
        @y1 = @x1
      end
    end

    # :ditto:
    @[AlwaysInline]
    def process(sample : Float32) : Float32
      self.process(sample.to_f64!).to_f32!
    end

    # "Runs" the filter over `block`.
    def process(block : Array(Float64)|Slice(Float64)) : BiQuadFilter
      if @active
        block.size.times do |t|
          # We can use the unsafe_ methods here since we are working on block.size.
          block.unsafe_put(t, process!(block.unsafe_fetch(t)))
        end
      else
        # These are always guaranteed to be safe.
        @x2 = block.unsafe_fetch(block.size - 2)
        @x1 = block.unsafe_fetch(block.size - 1)
        @y2 = @x2
        @y1 = @x1
      end

      self
    end

    # :ditto:
    def process(block : Array(Float32)|Slice(Float32)) : BiQuadFilter
      if @active
        block.size.times do |t|
          # We can use the unsafe_ methods here since we are working on block.size.
          block.unsafe_put(t, process!(block.unsafe_fetch(t)))
        end
      else
        # These are always guaranteed to be safe
        @x2 = block.unsafe_fetch(block.size - 2).to_f64!
        @x1 = block.unsafe_fetch(block.size - 1).to_f64!
        @y2 = @x2
        @y1 = @x1
      end

      self
    end

    # Generates a string that can be passed to a program to plot this filter on
    # a graph.  The `PlotType` dictates what kind of script is generated.
    def plot(pt : PlotType, *, coeffsOnly : Bool = false) : String
      # Adapted from what SoX prints out.
      String.build do |str|
        case pt
        in .gnu_plot?
          if coeffsOnly
            str << sprintf("b0 = %.15e; ", @b0)
            str << sprintf("b1 = %.15e; ", @b1)
            str << sprintf("b2 = %.15e; ", @b2)
            str << sprintf("a1 = %.15e; ", @a1)
            str << sprintf("a2 = %.15e", @a2)
          else
            str << "# gnuplot file\n"
            str << "set title 'BiQuad Filter, sample rate: #{@sampleRate}'\n"
            str << "set xlabel 'Frequency (Hz)'\n"
            str << "set ylabel 'Amplitude Response (dB)'\n"
            str << sprintf("Fs = %g\n", @sampleRate)
            str << sprintf("b0 = %.15e; ", @b0)
            str << sprintf("b1 = %.15e; ", @b1)
            str << sprintf("b2 = %.15e; ", @b2)
            str << sprintf("a1 = %.15e; ", @a1)
            str << sprintf("a2 = %.15e\n", @a2)
            str << "o = 2 * pi / Fs\n"
            str << "H(f) = sqrt((b0 * b0 + b1 * b1 + b2 * b2 + 2.0 * (b0 * b1 + b1 * b2) * " <<
              "cos(f * o) + 2.0 * (b0 * b2) * cos(2.0 * f * o)) / " <<
              "(1.0 + a1 * a1 + a2 * a2 + 2.0 * (a1 + a1 * a2) * " <<
              "cos(f * o) + 2.0 * a2 * cos(2.0 * f * o)))\n"
            str << "set logscale x\n"
            str << "set samples 250\n"
            str << "set grid xtics ytics\n"
            str << "set key off\n"
            str << "plot [f=10 : Fs/2] [-35:25] 20 * log10(H(f))"
          end
        in .octave?
          str << "%% GNU Octave file (may also work with MATLAB(R) )\n"
          str << "Fs = #{@sampleRate};\n"
          str << "minF = 10;\n"
          str << "maxF = Fs/2;\n"
          str << "sweepF = logspace(log10(minF), log10(maxF), 200);\n"
          str << sprintf("[h, w] = freqz([%.15e %.15e %.15e], [1 %.15e %.15e], sweepF, Fs);\n",
                         @b0, @b1, @b2, @a1, @a2)
          str << "semilogx(w, 20 * log10(h))\n"
          str << "title('BiQuad Filter, sample rate: #{@sampleRate}')\n"
          str << "xlabel('Frequency (Hz)')\n"
          str << "ylabel('Amplitude Response (dB)')\n"
          str << "axis([minF maxF -35 25])\n"
          str << "grid on\n"
          str << "disp('Hit return to continue')\n"
          str << "pause\n"
        end
      end
    end
  end
end
