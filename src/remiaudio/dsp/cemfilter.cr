#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./lpfilter"

###
### Some calculations taken from this code, by Aaron Giles:
### https://github.com/mamedev/mame/blob/master/src/devices/sound/cem3394.cpp
###
### The original code offers two different sets of calculations, both of which
### are reflected here.  Passing -Daltcemfilter during compilation will enable
### the less stable/slightly buggier set of calculations, while omitting it will
### enable the higher quality ones (the default).
###

module RemiAudio::DSP
  # Implements a lowpass filter that is similar to the one in a Curtis CEM3394.
  class CemFilter
    include ::RemiAudio::DSP::LPFilter

    getter b0 : Float64 = 0.0
    getter b1 : Float64 = 0.0
    getter b2 : Float64 = 0.0
    getter b3 : Float64 = 0.0
    getter b4 : Float64 = 0.0
    getter a0 : Float64 = 0.0
    getter a1 : Float64 = 0.0
    getter a2 : Float64 = 0.0
    getter a3 : Float64 = 0.0
    getter a4 : Float64 = 0.0

    @historyIn : Array(Float64) = Array(Float64).new(4, 0.0)
    @historyOut : Array(Float64) = Array(Float64).new(4, 0.0)

    # Creates a new `CemFilter`.
    def initialize(sampleRate)
      @sampleRate = sampleRate.to_f64!
      @invSampleRate = 1.0 / @sampleRate
      updateCoefficients
    end

    # Clears the internal buffers.
    @[AlwaysInline]
    def reset : Nil
      @historyIn.fill(0.0)
      @historyOut.fill(0.0)
    end

    # Sets the cutoff frequency of the filter.  If this is less than the NyQuist
    # limit, then the filter will be disabled.
    @[AlwaysInline]
    def cutoff=(val : Float64)
      return if val == @cutoff
      @cutoff = val.clamp(CUTOFF_MIN, CUTOFF_MAX)
      updateCoefficients
    end

    # Sets the resonance of the filter.  This will be clamped to
    # `RESONANCE_MIN..RESONANCE_MAX`.
    @[AlwaysInline]
    def resonance=(val : Float64)
      return if val == @resonance
      @resonance = val.clamp(RESONANCE_MIN, RESONANCE_MAX)
      updateCoefficients
    end

    # Sets both the cutoff frequency and resonance of the filter.  If the cutoff
    # is less than the NyQuist limit, then the filter will be disabled.  The
    # cutoff will be clamped to `RESONANCE_MIN..RESONANCE_MAX`.
    @[AlwaysInline]
    def set(newCutoff : Float64, newResonance : Float64)
      return if @cutoff == newCutoff && @resonance == newResonance
      @cutoff = newCutoff.clamp(CUTOFF_MIN, CUTOFF_MAX)
      @resonance = newResonance.clamp(RESONANCE_MIN, RESONANCE_MAX)
      updateCoefficients
    end

    @outScale : Float64 = 1.0

    def updateCoefficients : Nil
      @active = @cutoff <= ENABLE_AT

      {% begin %}
        {% if flag?(:altcemfilter) %}
          omega : Float64 = RemiMath::TWO_PI * @cutoff * @invSampleRate
          zc : Float64 = omega / Math.tan(omega * 0.5)
          ozc : Float64 = zc / omega
          ozc2 : Float64 = ozc * ozc
          ozc3 : Float64 = ozc2 * ozc
          ozc4 : Float64 = ozc3 * ozc

          resP1 = @resonance * 4 + 1.0
          @a0 = resP1
          @a1 = 4 * resP1
          @a2 = 6 * resP1
          @a3 = 4 * resP1
          @a4 = resP1

          @b0 =      resP1 + 4 * ozc + 6 * ozc2 + 4 * ozc3 + ozc4
          @b1 = 4 * (resP1 + 2 * ozc            - 2 * ozc3 - ozc4)
          @b2 = 6 * (resP1           - 2 * ozc2            + ozc4)
          @b3 = 4 * (resP1 - 2 * ozc            + 2 * ozc3 - ozc4)
          @b4 =      resP1 - 4 * ozc + 6 * ozc2 - 4 * ozc3 + ozc4

        {% else %}
          @outScale = if @resonance > 0.99
                        @resonance = 0.99
                        0.5
                      else
                        1.0
                      end
          g : Float64 = Math.tan(Math::PI * @cutoff * @invSampleRate)
          k : Float64 = 2.0 - 2.0 * @resonance
          @a1 = 1.0 / (1.0 + g * (g + k))
          @a2 = g * @a1
          @a3 = g * @a2
        {% end %}
      {% end %}
    end

    # Processes a single sample with the filter, returning a new sample.
    def process(sample : Float64) : Float64
      return sample unless @active
      {% begin %}
        {% if flag?(:altcemfilter) %}
          output = (sample * @a0 +
                    @historyIn[0] * @a1 +  @historyIn[1] * @a2 +  @historyIn[2] * @a3 +  @historyIn[3] * @a4 -
                    @historyOut[0] * @b1 - @historyOut[1] * @b2 - @historyOut[2] * @b3 - @historyOut[3] * @b4) / @b0
          output = 0.0 if output == Float64::NAN
          output = 10.0 if output > 10.0

          @historyIn[3] = @historyIn[2]
          @historyIn[2] = @historyIn[1]
          @historyIn[1] = @historyIn[0]
          @historyIn[0] = sample

          @historyOut[3] = @historyOut[2]
          @historyOut[2] = @historyOut[1]
          @historyOut[1] = @historyOut[0]
          @historyOut[0] = output

          output.clamp(-1.0, 1.0)

        {% else %}
          v3 : Float64 = sample - @historyOut[1]
          v1 : Float64 = @a1 * @historyOut[0] + @a2 * v3
          v2 : Float64 = @historyOut[1] + @a2 * @historyOut[0] + @a3 * v3
          @historyOut[0] = 2 * v1 - @historyOut[0]
          @historyOut[1] = 2 * v2 - @historyOut[1]

          # lowpass output is equal to v2
          output : Float64 = v2 * @outScale

          # Handle NaN's
          if output == Float64::NAN
            output = 0.0
            @historyOut[0] = 0
            @historyOut[1] = 0
          elsif output.abs > 1.0
            # If we go out of range, scale down to 1.0 and also scale our feedback
            # terms to help us stay in control.
            #scale : Float64 = 1.0 / output.abs
            #output *= scale
            #@historyOut[0] *= scale
            #@historyOut[1] *= scale
          end

          output
        {% end %}
      {% end %}
    end

    # Generates a string that can be passed to a program to plot this filter on
    # a graph.  The `PlotType` dictates what kind of script is generated.
    # def plot(pt : PlotType, *, coeffsOnly : Bool = false) : String
    #   # Adapted from what SoX prints out.
    #   String.build do |str|
    #     case pt
    #     in .gnu_plot?
    #       if coeffsOnly
    #         str << sprintf("b0 = %.15e; ", @b0)
    #         str << sprintf("b1 = %.15e; ", @b1)
    #         str << sprintf("b2 = %.15e; ", @b2)
    #         str << sprintf("a1 = %.15e; ", @a1)
    #         str << sprintf("a2 = %.15e", @a2)
    #       else
    #         str << "# gnuplot file\n"
    #         str << "set title 'CemFilter, sample rate: #{@sampleRate}'\n"
    #         str << "set xlabel 'Frequency (Hz)'\n"
    #         str << "set ylabel 'Amplitude Response (dB)'\n"
    #         str << sprintf("Fs = %g\n", @sampleRate)
    #         str << sprintf("b0 = %.15e; ", @b0)
    #         str << sprintf("b1 = %.15e; ", @b1)
    #         str << sprintf("b2 = %.15e; ", @b2)
    #         str << sprintf("a1 = %.15e; ", @a1)
    #         str << sprintf("a2 = %.15e\n", @a2)
    #         str << "o = 2 * pi / Fs\n"
    #         str << "H(f) = sqrt((b0 * b0 + b1 * b1 + b2 * b2 + 2.0 * (b0 * b1 + b1 * b2) * " <<
    #           "cos(f * o) + 2.0 * (b0 * b2) * cos(2.0 * f * o)) / " <<
    #           "(1.0 + a1 * a1 + a2 * a2 + 2.0 * (a1 + a1 * a2) * " <<
    #           "cos(f * o) + 2.0 * a2 * cos(2.0 * f * o)))\n"
    #         str << "set logscale x\n"
    #         str << "set samples 250\n"
    #         str << "set grid xtics ytics\n"
    #         str << "set key off\n"
    #         str << "plot [f=10 : Fs/2] [-35:25] 20 * log10(H(f))"
    #       end
    #     in .octave?
    #       str << "%% GNU Octave file (may also work with MATLAB(R) )\n"
    #       str << "Fs = #{@sampleRate};\n"
    #       str << "minF = 10;\n"
    #       str << "maxF = Fs/2;\n"
    #       str << "sweepF = logspace(log10(minF), log10(maxF), 200);\n"
    #       str << sprintf("[h, w] = freqz([%.15e %.15e %.15e], [1 %.15e %.15e], sweepF, Fs);\n",
    #                      @b0, @b1, @b2, @a1, @a2)
    #       str << "semilogx(w, 20 * log10(h))\n"
    #       str << "title('CemFilter, sample rate: #{@sampleRate}')\n"
    #       str << "xlabel('Frequency (Hz)')\n"
    #       str << "ylabel('Amplitude Response (dB)')\n"
    #       str << "axis([minF maxF -35 25])\n"
    #       str << "grid on\n"
    #       str << "disp('Hit return to continue')\n"
    #       str << "pause\n"
    #     end
    #   end
    # end
  end
end
