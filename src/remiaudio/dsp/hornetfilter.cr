#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.
require "./lpfilter"

###
### Some calculations taken from this code, by Joep Vanlier:
### https://github.com/JoepVanlier/JSFX/blob/master/Filther/Filther.jsfx
###

module RemiAudio::DSP
  # Implements an aggressive lowpass filter.  Sorta kinda an approximation of
  # the filter in an EDP Wasp.
  class HornetFilter
    include ::RemiAudio::DSP::LPFilter

    @d1 : Float64 = 0.0
    @d2 : Float64 = 0.0
    @di : Float64 = 0.0
    @v1 : Float64 = 0.0
    @v2 : Float64 = 0.0

    @nl : Float64 = 0.0
    @hah : Float64 = 0.0
    @hahnl : Float64 = 0.0
    @r : Float64 = 0.0
    @offset : Float64 = 0.0

    def initialize(sampleRate)
      @sampleRate = sampleRate.to_f64!
      @invSampleRate = 1.0 / @sampleRate
      updateCoefficients
    end

    @[AlwaysInline]
    def updateCoefficients : Nil
      h = 1.0 / @sampleRate
      @offset = 0.01
      @active = @cutoff <= ENABLE_AT
      omega = RemiMath::TWO_PI * @cutoff * @invSampleRate * 0.5
      f = 40 * @sampleRate * Math.tan(omega * 0.5)
      @r = 0.8 - 0.6 * @resonance
      @nl = 0.35
      a = 0.08 * f
      @hah = 0.5 * a * h
      @hahnl = @hah * @nl
    end

    # Clears the internal buffers.
    @[AlwaysInline]
    def reset : Nil
      @d1 = 0.0
      @d2 = 0.0
      @di = 0.0
      @v1 = 0.0
      @v2 = 0.0
    end

    # Sets the cutoff frequency of the filter.  If this is less than the NyQuist
    # limit, then the filter will be disabled.
    @[AlwaysInline]
    def cutoff=(val : Float64)
      return if val == @cutoff
      @cutoff = val.clamp(CUTOFF_MIN, CUTOFF_MAX)
      updateCoefficients
    end

    # Sets the resonance of the filter.  This will be clamped to
    # `RESONANCE_MIN..RESONANCE_MAX`.
    @[AlwaysInline]
    def resonance=(val : Float64)
      return if val == @resonance
      @resonance = val.clamp(RESONANCE_MIN, RESONANCE_MAX)
      updateCoefficients
    end

    # Sets both the cutoff frequency and resonance of the filter.  If the cutoff
    # is less than the NyQuist limit, then the filter will be disabled.  The
    # cutoff will be clamped to `RESONANCE_MIN..RESONANCE_MAX`.
    @[AlwaysInline]
    def set(newCutoff : Float64, newResonance : Float64)
      return if @cutoff == newCutoff && @resonance == newResonance
      @cutoff = newCutoff.clamp(CUTOFF_MIN, CUTOFF_MAX)
      @resonance = newResonance.clamp(RESONANCE_MIN, RESONANCE_MAX)
      updateCoefficients
    end

    def process(sample : Float64) : Float64
      return sample unless @active
      sample  = sample * 0.4
      tv1nl   = RemiMath.fastTanh(@v1 * @nl + @offset)
      tv2nl   = RemiMath.fastTanh(@v2 * @nl + @offset)
      td1nl   = RemiMath.fastTanh(@d1 * @nl + @offset)
      td2nl   = RemiMath.fastTanh(@d2 * @nl + @offset)
      ttd1nl  = RemiMath.fastTanh(td1nl)
      fixterm = RemiMath.fastTanh(@nl * (@di + (-@r * td1nl) - td2nl))

      3.times do |_|
        resclamp = -@r * tv1nl
        clterm  = @nl * (sample + resclamp - tv2nl)
        f0      = -@d1 + @v1 - @hah * (fixterm + clterm)
        f1      = -@d2 + @v2 - @hah * (ttd1nl + tv1nl)

        shrterm = @hahnl * @nl * (1 - clterm * clterm) * (1 - clterm * clterm)
        jb      = shrterm * (1 - tv2nl * tv2nl)
        ja      = @r * shrterm * (1 - tv1nl * tv1nl) + 1
        jc      = -@hahnl * (1 - tv1nl * tv1nl) * (1 - tv1nl * tv1nl)

        idet    = 1.0 / (ja - jb * jc)
        @v1      = @v1 - (-jb * f1 + f0) * idet
        @v2      = @v2 - (ja * f1 - jc * f0) * idet

        tv1nl   = RemiMath.fastTanh(@v1 * @nl)
        tv2nl   = RemiMath.fastTanh(@v2 * @nl)
      end

      @di = sample
      @d1 = @v1
      @d2 = @v2

      -@v2
    end
  end
end
