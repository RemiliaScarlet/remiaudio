#### RemiAudio
#### Copyright (C) 2022-2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

module RemiAudio::DSP
  # A filter that blocks DC bias.
  class DCFilter

    @input : Float64 = 0.0
    @output : Float64 = 0.0

    # Creates a new `DCFilter` instance.
    def initialize
    end

    # Processes a single sample.
    @[AlwaysInline]
    def process(sample : Float64) : Float64
      @output = (sample - @input) + ((0.999f64 - 0.01f64 * 0.4f64) * @output)
      @input = sample
    end

    # :ditto:
    @[AlwaysInline]
    def process(sample : Float32) : Float32
      process(sample.to_f64!).to_f32!
    end

    # Resets the filter
    @[AlwaysInline]
    def reset : Nil
      @input = 0.0
      @output = 0.0
    end
  end
end
