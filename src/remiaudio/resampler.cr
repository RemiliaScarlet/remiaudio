#### Based on libsamplerate
####
#### Copyright (c) 2002-2021, Erik de Castro Lopo <erikd@mega-nerd.com>
#### Copyright (c) 2024, Remilia Scarlet <remilia@posteo.jp>
#### All rights reserved.
####
#### This code is released under 2-clause BSD license. Please see the
#### file at : https://github.com/libsndfile/libsamplerate/blob/master/COPYING
require "./common"

# The `RemiAudio::Resampler` module provides fast, high-quality resampling based
# on libsamplerate.  This is not a set of bindings to libsamplerate, but rather
# a port of the code to native Crystal for easier use.
#
# There are two APIs that you can use for resampling: the "Normal" API (called
# the "Full API" in libsamplerate documentation) and the "Callback" API.
module RemiAudio::Resampler
  # :nodoc:
  SRC_MAX_RATIO = 256

  # :nodoc:
  SRC_MIN_RATIO_DIFF = 1e-20

  enum Type
    SincBest
    SincMedium
    SincFastest
    ZeroOrderHold
    Linear
  end

  # Returns `true` if *value* is a valid ratio for this library, or `false`
  # otherwise.
  @[AlwaysInline]
  def self.validRatio?(value : Float64) : Bool
    !(value < (1.0 / SRC_MAX_RATIO) ||
      value > (1.0 * SRC_MAX_RATIO))
  end

  class Error < ::RemiAudio::RemiAudioError
  end

  class BadRatioError < Error
  end

  alias CallbackProc = Proc(Tuple(Slice(Float32), Int64))
end

require "./resampler/*"
