#### RemiAudio
#### Copyright (C) 2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

####
#### PortAudio Bindings
####

@[Link("portaudio")]
lib LibPortAudio
  FORMAT_IS_SUPPORTED = 0
  FRAMES_PER_BUFFER_UNSPECIFIED = 0
  NO_DEVICE = -1
  USE_HOST_API_SPECIFIC_DEVICE_SPECIFICATION = -2

  alias PaStreamT = Void*
                    alias HostApiIndex = LibC::Int
  alias PPaStream = PaStreamT
  alias DeviceIndex = LibC::Int
  alias PaTime = LibC::Double
  alias PHostErrorInfo = HostErrorInfo*
                         alias PDeviceInfo = DeviceInfo*
                                             alias PStreamParameters = StreamParameters*
                                                                       alias PHostApiInfo = HostApiInfo*
                                                                                            alias PStreamInfo = StreamInfo*
                                                                                                                alias StreamCallbackFlags = LibC::ULong

  enum HostApiType
    InDevelopment = 0
    DirectSound
    Mme
    Asio
    SoundManager
    CoreAudio
    Oss
    Alsa
    Al
    BeOs
    Wdmks
    Jack
    Wasapi
    AudioScienceHpi
  end

  alias SampleFormat = LibC::ULong

  alias PaCallback = Proc(Pointer(Void),                   # Input buffer
                          Pointer(Void),                   # Output buffer
                          LibC::ULong,                     # Frames per buffer
                          Pointer(StreamCallbackTimeInfo), # Timing info
                          StreamCallbackFlags,             # Flags
                          Pointer(Void),                   # User data
                          LibC::Int)                       # Return value

  @[Flags]
  enum StreamFlags
    NoFlag = 0
    ClipOff = 0x00000001
    DitherOff = 0x00000002
  end

  enum PaError
    NoError = 0
    NotInitialized = -10000
    UnanticipatedHostError
    InvalidChannelCount
    InvalidSampleRate
    InvalidDevice
    InvalidFlag
    SampleFormatNotSupported
    BadIODeviceCombination
    InsufficientMemory
    BufferTooBig
    BufferTooSmall
    NullCallback
    BadStreamPtr
    TimedOut
    InternalError
    DeviceUnavailable
    IncompatibleHostApiSpecificStreamInfo
    StreamIsStopped
    StreamIsNotStopped
    InputOverflowed
    OutputUnderflowed
    HostApiNotFound
    InvalidHostApi
    CanNotReadFromACallbackStream
    CanNotWriteToACallbackStream
    CanNotReadFromAnOutputOnlyStream
    CanNotWriteToAnInputOnlyStream
    IncompatibleStreamHostApi
    BadBufferPtr
  end

  struct HostErrorInfo
    hostApiTypeID : HostApiType
    errorCode : LibC::Long
    errorText : LibC::Char*
  end

  struct DeviceInfo
    structVersion : LibC::Int
    name : LibC::Char*
      hostApi : HostApiIndex
    maxInputChannels : LibC::Int
    maxOutputChannels : LibC::Int
    defaultLowInputLatency : PaTime
    defaultLowOutputLatency : PaTime
    defaultHighInputLatency : PaTime
    defaultHighOutputLatency : PaTime
    defaultSampleRate : LibC::Double
  end

  struct StreamParameters
    device : DeviceIndex
    channelCount : LibC::Int
    sampleFormat : SampleFormat
    suggestedLatency : PaTime
    hostApiSpecificStreamInfo : Void*
  end

  struct HostApiInfo
    structVersion : LibC::Int
    typeId : HostApiType
    name : LibC::Char*
      deviceCount : LibC::Int
    defaultInputDevice : DeviceIndex
    defaultOutputDevice : DeviceIndex
  end

  struct StreamInfo
    structVersion : LibC::Int
    inputLatency : PaTime
    outputLatency : PaTime
    sampleRate : LibC::Double
  end

  struct StreamCallbackTimeInfo
    inputBufferAdcTime : PaTime
    currentTime : PaTime
    outputBufferDacTime : PaTime
  end

  fun Pa_GetErrorText(errCode : LibC::Int) : LibC::Char*
      fun Pa_GetLastHostErrorInfo() : PHostErrorInfo
  fun Pa_GetVersion() : LibC::Int
  fun Pa_GetVersionText() : LibC::Char*
      fun Pa_GetDeviceCount() : DeviceIndex
  fun Pa_GetDefaultOutputDevice() : DeviceIndex
  fun Pa_GetDeviceInfo(device : DeviceIndex) : PDeviceInfo
  fun Pa_IsFormatSupported(inputParams : StreamParameters*, outputParams : StreamParameters*,
                                                                                           sampleRate : LibC::Double) : PaError
  fun Pa_HostApiTypeIdToHostApiIndex(typeId : HostApiType) : HostApiIndex
  fun Pa_GetDefaultInputDevice() : DeviceIndex
  fun Pa_GetHostApiInfo(hostApi : HostApiIndex) : PHostApiInfo
  fun Pa_Initialize() : PaError
  fun Pa_Terminate() : PaError
  fun Pa_GetDefaultHostApi() : HostApiIndex
  fun Pa_OpenDefaultStream(paStream : PPaStream, numInputChannels : LibC::Int, numOutputChannels : LibC::Int,
                           sampleFormat : LibC::ULong, sampleRate : LibC::Double, framesPerBuffer : LibC::ULong,
                           streamCallback : PaCallback, userData : Void*) : PaError
  fun Pa_OpenStream(paStream : PPaStream, inputParams : PStreamParameters*, outputParams : PStreamParameters*,
                                                                                                            sampleRate : LibC::Double, framesPerBuffer : LibC::ULong, streamFlags : StreamFlags,
                    streamCallback : PaCallback, userData : Void*)
  fun Pa_CloseStream(paStream : PPaStream) : PaError
  fun Pa_IsStreamStopped(paStream : PPaStream) : PaError
  fun Pa_GetStreamTime(paStream : PPaStream) : PaTime
  fun Pa_GetHostApiCount() : HostApiIndex
  fun Pa_IsStreamActive(paStream : PPaStream) : LibC::Int
  fun Pa_GetStreamWriteAvailable(paStream : PPaStream) : LibC::Long
  fun Pa_GetStreamReadAvailable(paStream : PPaStream) : LibC::Long
  fun Pa_ReadStream(paStream : PPaStream, buffer : Void*, frames : LibC::ULong) : PaError
  fun Pa_WriteStream(paStream : PPaStream, buffer : Void*, frames : LibC::ULong) : PaError
  fun Pa_StartStream(paStream : PPaStream) : PaError
  fun Pa_AbortStream(paStream : PPaStream) : PaError
  fun Pa_StopStream(paStream : PPaStream) : PaError
  fun Pa_GetStreamInfo(paStream : PPaStream) : PStreamInfo
  fun Pa_GetSampleSize(fmt : SampleFormat) : LibC::Int
  fun Pa_HostApiDeviceIndexToDeviceIndex(hostApi : HostApiIndex, hostApiDeviceIndex : LibC::Int) : DeviceIndex
  fun Pa_Sleep(msec : LibC::Long) : Void
end
