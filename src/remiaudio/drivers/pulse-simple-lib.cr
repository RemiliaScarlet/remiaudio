#### RemiAudio
#### Copyright (C) 2024 Remilia Scarlet <remilia@posteo.jp>
####
#### This program is free software: you can redistribute it and/or modify it
#### under the terms of the GNU Affero General Public License as published by
#### the Free Software Foundation, either version 3 of the License, or (at your
#### option) any later version.
####
#### This program is distributed in the hope that it will be useful, but WITHOUT
#### ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#### FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
#### License for more details.
####
#### You should have received a copy of the GNU Affero General Public License
#### along with this program.  If not, see <https://www.gnu.org/licenses/>.

####
#### Bindings to the "simple" PulseAudio API.
####

@[Link(ldflags: "`pkg-config libpulse-simple --libs`")]
lib LibPulseSimple
  alias PaSimple = Pointer(Void)

  enum PaSampleFormat
    PA_SAMPLE_U8
    PA_SAMPLE_ALAW
    PA_SAMPLE_ULAW
    PA_SAMPLE_S16LE
    PA_SAMPLE_S16BE
    PA_SAMPLE_FLOAT32LE
    PA_SAMPLE_FLOAT32BE
    PA_SAMPLE_S32LE
    PA_SAMPLE_S32BE
    PA_SAMPLE_S24LE
    PA_SAMPLE_S24BE
    PA_SAMPLE_S24_32LE
    PA_SAMPLE_S24_32BE
    PA_SAMPLE_MAX
    PA_SAMPLE_INVALID = -1
  end

  enum PaStreamDirection
    PA_STREAM_NODIRECTION
    PA_STREAM_PLAYBACK
    PA_STREAM_RECORD
    PA_STREAM_UPLOAD
  end

  enum PaChannelPosition
    PA_CHANNEL_POSITION_INVALID = -1
    PA_CHANNEL_POSITION_MONO = 0
    PA_CHANNEL_POSITION_FRONT_LEFT
    PA_CHANNEL_POSITION_FRONT_RIGHT
    PA_CHANNEL_POSITION_FRONT_CENTER
    PA_CHANNEL_POSITION_LEFT = PA_CHANNEL_POSITION_FRONT_LEFT
    PA_CHANNEL_POSITION_RIGHT = PA_CHANNEL_POSITION_FRONT_RIGHT
    PA_CHANNEL_POSITION_CENTER = PA_CHANNEL_POSITION_FRONT_CENTER
    PA_CHANNEL_POSITION_REAR_CENTER
    PA_CHANNEL_POSITION_REAR_LEFT
    PA_CHANNEL_POSITION_REAR_RIGHT
    PA_CHANNEL_POSITION_LFE
    PA_CHANNEL_POSITION_SUBWOOFER = PA_CHANNEL_POSITION_LFE
    PA_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER
    PA_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER
    PA_CHANNEL_POSITION_SIDE_LEFT
    PA_CHANNEL_POSITION_SIDE_RIGHT
    PA_CHANNEL_POSITION_AUX0
    PA_CHANNEL_POSITION_AUX1
    PA_CHANNEL_POSITION_AUX2
    PA_CHANNEL_POSITION_AUX3
    PA_CHANNEL_POSITION_AUX4
    PA_CHANNEL_POSITION_AUX5
    PA_CHANNEL_POSITION_AUX6
    PA_CHANNEL_POSITION_AUX7
    PA_CHANNEL_POSITION_AUX8
    PA_CHANNEL_POSITION_AUX9
    PA_CHANNEL_POSITION_AUX10
    PA_CHANNEL_POSITION_AUX11
    PA_CHANNEL_POSITION_AUX12
    PA_CHANNEL_POSITION_AUX13
    PA_CHANNEL_POSITION_AUX14
    PA_CHANNEL_POSITION_AUX15
    PA_CHANNEL_POSITION_AUX16
    PA_CHANNEL_POSITION_AUX17
    PA_CHANNEL_POSITION_AUX18
    PA_CHANNEL_POSITION_AUX19
    PA_CHANNEL_POSITION_AUX20
    PA_CHANNEL_POSITION_AUX21
    PA_CHANNEL_POSITION_AUX22
    PA_CHANNEL_POSITION_AUX23
    PA_CHANNEL_POSITION_AUX24
    PA_CHANNEL_POSITION_AUX25
    PA_CHANNEL_POSITION_AUX26
    PA_CHANNEL_POSITION_AUX27
    PA_CHANNEL_POSITION_AUX28
    PA_CHANNEL_POSITION_AUX29
    PA_CHANNEL_POSITION_AUX30
    PA_CHANNEL_POSITION_AUX31
    PA_CHANNEL_POSITION_TOP_CENTER
    PA_CHANNEL_POSITION_TOP_FRONT_LEFT
    PA_CHANNEL_POSITION_TOP_FRONT_RIGHT
    PA_CHANNEL_POSITION_TOP_FRONT_CENTER
    PA_CHANNEL_POSITION_TOP_REAR_LEFT
    PA_CHANNEL_POSITION_TOP_REAR_RIGHT
    PA_CHANNEL_POSITION_TOP_REAR_CENTER
    PA_CHANNEL_POSITION_MAX
  end

  struct PaSampleSpec
    format : PaSampleFormat
    rate : UInt32
    channels : UInt8
  end

  struct PaChannelMap
    channels : UInt8
    map : PaChannelPosition[32]
  end

  struct PaBufferAttr
    maxLength : UInt32
    tLength : UInt32
    preBuf : UInt32
    minReq : UInt32
    fragSize : UInt32
  end

  fun pa_simple_new(server : LibC::Char*, name : LibC::Char*, dir : PaStreamDirection, dev : LibC::Char*,
                                                                                                       streamName : LibC::Char*, spec : PaSampleSpec*, map : PaChannelMap*, attr : PaBufferAttr*,
                                                                                                                                                                                               error : LibC::Int*) : PaSimple
  fun pa_simple_free(simple : PaSimple) : Void
  fun pa_simple_write(simple : PaSimple, data : Void*, len : LibC::SizeT, err : LibC::Int*) : LibC::Int
  fun pa_simple_drain(simple : PaSimple, err : LibC::Int*) : LibC::Int
  fun pa_strerror(err : LibC::Int) : LibC::Char*

      fun pa_get_library_version() : LibC::Char*
end
