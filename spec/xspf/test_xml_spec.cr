require "../spec_helper"

describe "XML Reading and Writing" do
  it "correctly reads a valid XSPF document" do
    playlist = File.open("./spec/xspf/valid-playlist.xspf") do |file|
      RemiAudio::Xspf::Playlist.read(file)
    end

    playlist.title.should eq "Title"
    playlist.creator.should eq "Creator"
    playlist.annotate.should eq "Annotation"

    playlist.info.should_not be_nil
    playlist.info.try do |value|
      value.to_s.should eq "http://www.example.org/"
    end

    playlist.identifier.should_not be_nil
    playlist.identifier.try do |value|
      value.to_s.should eq "urn:00:0"
    end

    playlist.image.should_not be_nil
    playlist.image.try do |value|
      value.to_s.should eq "http://www.example.org/"
    end

    playlist.license.should_not be_nil
    playlist.license.try do |value|
      value.to_s.should eq "http://www.example.org/"
    end

    playlist.attribution.should_not be_nil
    playlist.attribution.try do |value|
      value.locations.size.should eq 2
      value.locations[0].to_s.should eq "http://www.example.org/first"
      value.locations[1].to_s.should eq "http://www.example.org/second"

      value.identifiers.size.should eq 1
      value.identifiers[0].to_s.should eq "urn:00:0"
    end

    playlist.links.size.should eq 2
    playlist.links[0].rel.to_s.should eq "http://www.example.org/"
    playlist.links[0].content.to_s.should eq "http://www.example.org/first"
    playlist.links[1].rel.to_s.should eq "http://www.example.org/"
    playlist.links[1].content.to_s.should eq "http://www.example.org/second"

    playlist.meta.size.should eq 2
    playlist.meta[0].rel.to_s.should eq "http://www.example.org/first"
    playlist.meta[0].content.to_s.should eq "first"
    playlist.meta[1].rel.to_s.should eq "http://www.example.org/second"
    playlist.meta[1].content.to_s.should eq "second"

    playlist.tracks.size.should eq 1
    track = playlist.tracks[0]
    track.locations.size.should eq 2
    track.locations[0].to_s.should eq "http://www.example.org/first"
    track.locations[1].to_s.should eq "http://www.example.org/second"
    track.identifiers.size.should eq 2
    track.identifiers[0].to_s.should eq "urn:00:1"
    track.identifiers[1].to_s.should eq "urn:00:2"
    track.title.should eq "Title"
    track.creator.should eq "Creator"
    track.annotate.should eq "Annotation"
    track.album.should eq "Album"

    track.trackNumber.should_not be_nil
    track.trackNumber.try do |value|
      value.should eq 14
    end

    track.duration.should_not be_nil
    track.duration.try do |value|
      value.should eq 123
    end

    track.info.should_not be_nil
    track.info.try do |value|
      value.to_s.should eq "http://www.example.org/"
    end

    track.image.should_not be_nil
    track.image.try do |value|
      value.to_s.should eq "relative_image_uri.png"
    end

    track.links.size.should eq 2
    track.links[0].rel.to_s.should eq "http://www.example.org/"
    track.links[0].content.to_s.should eq "http://www.example.org/first"
    track.links[1].rel.to_s.should eq "http://www.example.org/"
    track.links[1].content.to_s.should eq "http://www.example.org/second"

    track.meta.size.should eq 2
    track.meta[0].rel.to_s.should eq "http://www.example.org/first"
    track.meta[0].content.to_s.should eq "first"
    track.meta[1].rel.to_s.should eq "http://www.example.org/second"
    track.meta[1].content.to_s.should eq "second"
  end

  it "correctly writes a valid XSPF document" do
    playlist = File.open("./spec/xspf/valid-playlist.xspf") do |file|
      RemiAudio::Xspf::Playlist.read(file)
    end
    xmlStr = playlist.write
    playlist = RemiAudio::Xspf::Playlist.read(xmlStr)

    playlist.title.should eq "Title"
    playlist.creator.should eq "Creator"
    playlist.annotate.should eq "Annotation"

    playlist.info.should_not be_nil
    playlist.info.try do |value|
      value.to_s.should eq "http://www.example.org/"
    end

    playlist.identifier.should_not be_nil
    playlist.identifier.try do |value|
      value.to_s.should eq "urn:00:0"
    end

    playlist.image.should_not be_nil
    playlist.image.try do |value|
      value.to_s.should eq "http://www.example.org/"
    end

    playlist.license.should_not be_nil
    playlist.license.try do |value|
      value.to_s.should eq "http://www.example.org/"
    end

    playlist.attribution.should_not be_nil
    playlist.attribution.try do |value|
      value.locations.size.should eq 2
      value.locations[0].to_s.should eq "http://www.example.org/first"
      value.locations[1].to_s.should eq "http://www.example.org/second"

      value.identifiers.size.should eq 1
      value.identifiers[0].to_s.should eq "urn:00:0"
    end

    playlist.links.size.should eq 2
    playlist.links[0].rel.to_s.should eq "http://www.example.org/"
    playlist.links[0].content.to_s.should eq "http://www.example.org/first"
    playlist.links[1].rel.to_s.should eq "http://www.example.org/"
    playlist.links[1].content.to_s.should eq "http://www.example.org/second"

    playlist.meta.size.should eq 2
    playlist.meta[0].rel.to_s.should eq "http://www.example.org/first"
    playlist.meta[0].content.to_s.should eq "first"
    playlist.meta[1].rel.to_s.should eq "http://www.example.org/second"
    playlist.meta[1].content.to_s.should eq "second"

    playlist.tracks.size.should eq 1
    track = playlist.tracks[0]
    track.locations.size.should eq 2
    track.locations[0].to_s.should eq "http://www.example.org/first"
    track.locations[1].to_s.should eq "http://www.example.org/second"
    track.identifiers.size.should eq 2
    track.identifiers[0].to_s.should eq "urn:00:1"
    track.identifiers[1].to_s.should eq "urn:00:2"
    track.title.should eq "Title"
    track.creator.should eq "Creator"
    track.annotate.should eq "Annotation"
    track.album.should eq "Album"

    track.trackNumber.should_not be_nil
    track.trackNumber.try do |value|
      value.should eq 14
    end

    track.duration.should_not be_nil
    track.duration.try do |value|
      value.should eq 123
    end

    track.info.should_not be_nil
    track.info.try do |value|
      value.to_s.should eq "http://www.example.org/"
    end

    track.image.should_not be_nil
    track.image.try do |value|
      value.to_s.should eq "relative_image_uri.png"
    end

    track.links.size.should eq 2
    track.links[0].rel.to_s.should eq "http://www.example.org/"
    track.links[0].content.to_s.should eq "http://www.example.org/first"
    track.links[1].rel.to_s.should eq "http://www.example.org/"
    track.links[1].content.to_s.should eq "http://www.example.org/second"

    track.meta.size.should eq 2
    track.meta[0].rel.to_s.should eq "http://www.example.org/first"
    track.meta[0].content.to_s.should eq "first"
    track.meta[1].rel.to_s.should eq "http://www.example.org/second"
    track.meta[1].content.to_s.should eq "second"
  end

  it "correctly reads a small valid XSPF document" do
    playlist = File.open("./spec/xspf/valid-playlist-small.xspf") do |file|
      RemiAudio::Xspf::Playlist.read(file)
    end
    playlist.title.should eq "Title"
    playlist.creator.should be_nil
    playlist.annotate.should be_nil
    playlist.info.should be_nil
    playlist.identifier.should be_nil
    playlist.image.should be_nil
    playlist.license.should be_nil
    playlist.attribution.should be_nil
    playlist.links.size.should eq 0
    playlist.meta.size.should eq 0

    playlist.tracks.size.should eq 1
    track = playlist.tracks[0]
    track.locations.size.should eq 1
    track.locations[0].to_s.should eq "http://www.example.org/first"
    track.identifiers.size.should eq 1
    track.identifiers[0].to_s.should eq "urn:00:1"
    track.title.should eq "Title"
    track.creator.should eq "Creator"
    track.album.should eq "Album"

    track.trackNumber.should_not be_nil
    track.trackNumber.try do |value|
      value.should eq 14
    end

    track.duration.should_not be_nil
    track.duration.try do |value|
      value.should eq 123
    end

    track.annotate.should be_nil
    track.info.should be_nil
    track.image.should be_nil
    track.links.size.should eq 0
    track.meta.size.should eq 0
  end

  it "correctly writes a small valid XSPF document" do
    playlist = File.open("./spec/xspf/valid-playlist-small.xspf") do |file|
      RemiAudio::Xspf::Playlist.read(file)
    end

    xmlStr = playlist.write
    playlist = RemiAudio::Xspf::Playlist.read(xmlStr)

    playlist.title.should eq "Title"
    playlist.creator.should be_nil
    playlist.annotate.should be_nil
    playlist.info.should be_nil
    playlist.identifier.should be_nil
    playlist.image.should be_nil
    playlist.license.should be_nil
    playlist.attribution.should be_nil
    playlist.links.size.should eq 0
    playlist.meta.size.should eq 0

    playlist.tracks.size.should eq 1
    track = playlist.tracks[0]
    track.locations.size.should eq 1
    track.locations[0].to_s.should eq "http://www.example.org/first"
    track.identifiers.size.should eq 1
    track.identifiers[0].to_s.should eq "urn:00:1"
    track.title.should eq "Title"
    track.creator.should eq "Creator"
    track.album.should eq "Album"

    track.trackNumber.should_not be_nil
    track.trackNumber.try do |value|
      value.should eq 14
    end

    track.duration.should_not be_nil
    track.duration.try do |value|
      value.should eq 123
    end

    track.annotate.should be_nil
    track.info.should be_nil
    track.image.should be_nil
    track.links.size.should eq 0
    track.meta.size.should eq 0
  end

  it "detects a missing <trackList> element" do
    xmlStr = <<-XML
<?xml version="1.0" encoding="UTF-8"?>

<playlist version="1" xmlns="http://xspf.org/ns/0/">
</playlist>
XML

    expect_raises(RemiAudio::Xspf::Error, "No <trackList> found in <playlist> node") do
      RemiAudio::Xspf::Playlist.read(xmlStr)
    end
  end
end
