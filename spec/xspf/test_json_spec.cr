require "../spec_helper"

describe "JSON Reading and Writing" do
  it "correctly reads a small valid JSPF document" do
    jsonStr = <<-JSON
 {
   "playlist" : {
     "title"         : "Two Songs From Thriller",
     "creator"       : "MJ Fan",
     "track"         : [
       {
         "location"      : ["http://example.com/billiejean.mp3"],
         "title"         : "Billie Jean",
         "creator"       : "Michael Jackson",
         "album"         : "Thriller"
       },
       {
       "location"      : ["http://example.com/thegirlismine.mp3"],
       "title"         : "The Girl Is Mine",
       "creator"       : "Michael Jackson",
       "album"         : "Thriller"
       }
     ]
   }
 }
JSON

    playlist = RemiAudio::Xspf::Playlist.from_json(jsonStr, root: "playlist")
    playlist.title.should_not be_nil
    playlist.title.not_nil!.should eq "Two Songs From Thriller"
    playlist.creator.should_not be_nil
    playlist.creator.not_nil!.should eq "MJ Fan"

    playlist.tracks.size.should eq 2
    track = playlist.tracks[0]
    track.locations.size.should eq 1
    track.locations[0].to_s.should eq "http://example.com/billiejean.mp3"
    track.title.should_not be_nil
    track.title.not_nil!.should eq "Billie Jean"
    track.creator.should_not be_nil
    track.creator.not_nil!.should eq "Michael Jackson"
    track.album.should_not be_nil
    track.album.not_nil!.should eq "Thriller"

    track = playlist.tracks[1]
    track.locations.size.should eq 1
    track.locations[0].to_s.should eq "http://example.com/thegirlismine.mp3"
    track.title.should_not be_nil
    track.title.not_nil!.should eq "The Girl Is Mine"
    track.creator.should_not be_nil
    track.creator.not_nil!.should eq "Michael Jackson"
    track.album.should_not be_nil
    track.album.not_nil!.should eq "Thriller"
  end

  it "correctly reads a large JSPF document" do
    playlist = File.open("./spec/xspf/valid-long.jspf") do |file|
      RemiAudio::Xspf::Playlist.read(file, format: RemiAudio::Xspf::Playlist::Format::Json)
    end
    playlist.title.should eq "JSPF example"
    playlist.creator.should eq "Name of playlist author"
    playlist.annotate.should eq "Super playlist"

    playlist.info.should_not be_nil
    playlist.info.try do |value|
      value.to_s.should eq "http://example.com/"
    end

    playlist.identifier.should_not be_nil
    playlist.identifier.try do |value|
      value.to_s.should eq "http://example.com/"
    end

    playlist.image.should_not be_nil
    playlist.image.try do |value|
      value.to_s.should eq "http://example.com/"
    end

    playlist.license.should_not be_nil
    playlist.license.try do |value|
      value.to_s.should eq "http://example.com/"
    end

    playlist.attribution.should_not be_nil
    playlist.attribution.try do |value|
      value.locations.size.should eq 1
      value.locations[0].to_s.should eq "http://example.com/"

      value.identifiers.size.should eq 1
      value.identifiers[0].to_s.should eq "http://example.com/"
    end

    playlist.links.size.should eq 2
    playlist.links[0].rel.to_s.should eq "http://example.com/rel/1/"
    playlist.links[0].content.to_s.should eq "http://example.com/body/1/"
    playlist.links[1].rel.to_s.should eq "http://example.com/rel/2/"
    playlist.links[1].content.to_s.should eq "http://example.com/body/2/"

    playlist.meta.size.should eq 2
    playlist.meta[0].rel.to_s.should eq "http://example.com/rel/1/"
    playlist.meta[0].content.to_s.should eq "my meta 69"
    playlist.meta[1].rel.to_s.should eq "http://example.com/rel/2/"
    playlist.meta[1].content.to_s.should eq "36"

    playlist.tracks.size.should eq 1
    track = playlist.tracks[0]
    track.locations.size.should eq 2
    track.locations[0].to_s.should eq "http://example.com/1.ogg"
    track.locations[1].to_s.should eq "http://example.com/2.mp3"
    track.identifiers.size.should eq 2
    track.identifiers[0].to_s.should eq "http://example.com/1/"
    track.identifiers[1].to_s.should eq "http://example.com/2/"
    track.title.should eq "Track title"
    track.creator.should eq "Artist name"
    track.annotate.should eq "Some text"
    track.album.should eq "Album name"

    track.trackNumber.should_not be_nil
    track.trackNumber.try do |value|
      value.should eq 1
    end

    track.duration.should_not be_nil
    track.duration.try do |value|
      value.should eq 0
    end

    track.info.should_not be_nil
    track.info.try do |value|
      value.to_s.should eq "http://example.com/"
    end

    track.image.should_not be_nil
    track.image.try do |value|
      value.to_s.should eq "http://example.com/"
    end

    track.links.size.should eq 2
    track.links[0].rel.to_s.should eq "http://example.com/rel/1/"
    track.links[0].content.to_s.should eq "http://example.com/body/1/"
    track.links[1].rel.to_s.should eq "http://example.com/rel/2/"
    track.links[1].content.to_s.should eq "http://example.com/body/2/"

    track.meta.size.should eq 2
    track.meta[0].rel.to_s.should eq "http://example.com/rel/1/"
    track.meta[0].content.to_s.should eq "my meta 36"
    track.meta[1].rel.to_s.should eq "http://example.com/rel/2/"
    track.meta[1].content.to_s.should eq "69"
  end

  it "Correctly writes JSPF" do
    jsonStr = <<-JSON
 {
   "playlist" : {
     "title"         : "Two Songs From Thriller",
     "creator"       : "MJ Fan",
     "track"         : [
       {
         "location"      : ["http://example.com/billiejean.mp3"],
         "title"         : "Billie Jean",
         "creator"       : "Michael Jackson",
         "album"         : "Thriller"
       },
       {
       "location"      : ["http://example.com/thegirlismine.mp3"],
       "title"         : "The Girl Is Mine",
       "creator"       : "Michael Jackson",
       "album"         : "Thriller"
       }
     ]
   }
 }
JSON

    playlist = RemiAudio::Xspf::Playlist.from_json(jsonStr, root: "playlist")
    newStr = playlist.write(format: RemiAudio::Xspf::Playlist::Format::Json)

    playlist = RemiAudio::Xspf::Playlist.from_json(newStr, root: "playlist")
    playlist.title.should_not be_nil
    playlist.title.not_nil!.should eq "Two Songs From Thriller"
    playlist.creator.should_not be_nil
    playlist.creator.not_nil!.should eq "MJ Fan"

    playlist.tracks.size.should eq 2
    track = playlist.tracks[0]
    track.locations.size.should eq 1
    track.locations[0].to_s.should eq "http://example.com/billiejean.mp3"
    track.title.should_not be_nil
    track.title.not_nil!.should eq "Billie Jean"
    track.creator.should_not be_nil
    track.creator.not_nil!.should eq "Michael Jackson"
    track.album.should_not be_nil
    track.album.not_nil!.should eq "Thriller"

    track = playlist.tracks[1]
    track.locations.size.should eq 1
    track.locations[0].to_s.should eq "http://example.com/thegirlismine.mp3"
    track.title.should_not be_nil
    track.title.not_nil!.should eq "The Girl Is Mine"
    track.creator.should_not be_nil
    track.creator.not_nil!.should eq "Michael Jackson"
    track.album.should_not be_nil
    track.album.not_nil!.should eq "Thriller"
  end
end
