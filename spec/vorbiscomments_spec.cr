require "./spec_helper"
require "../src/remiaudio/vorbiscomment"

describe "Vorbis Comments" do
  it "Reads Vorbis Comments from a stream" do
    comments = File.open("spec/vorbiscomment.raw", "rb") do |file|
      RemiAudio::VorbisComment.new(file)
    end

    comments.size.should eq 7
    comments.numKeys.should eq 6

    comments["title"].size.should eq 1
    comments["title"][0].should eq "Overdrive"

    comments["artist"].size.should eq 2
    comments["artist"][0].should eq "Partition36"
    comments["artist"][1].should eq "Alexa Jones-Gonzales"

    comments["DATE"].size.should eq 1
    comments["DATE"][0].should eq "2018"

    comments["TRACKTOTAL"].size.should eq 1
    comments["TRACKTOTAL"][0].should eq "01"

    comments["TRACKNUMBER"].size.should eq 1
    comments["TRACKNUMBER"][0].should eq "01"
  end

  it "Reads Vorbis Comments from bytes" do
    rawData = File.read("spec/vorbiscomment.raw").to_slice
    comments = RemiAudio::VorbisComment.new(rawData)

    comments.size.should eq 7
    comments.numKeys.should eq 6

    comments["title"].size.should eq 1
    comments["title"][0].should eq "Overdrive"

    comments["artist"].size.should eq 2
    comments["artist"][0].should eq "Partition36"
    comments["artist"][1].should eq "Alexa Jones-Gonzales"

    comments["DATE"].size.should eq 1
    comments["DATE"][0].should eq "2018"

    comments["TRACKTOTAL"].size.should eq 1
    comments["TRACKTOTAL"][0].should eq "01"

    comments["TRACKNUMBER"].size.should eq 1
    comments["TRACKNUMBER"][0].should eq "01"
  end

  it "Adds new comments" do
    comments = RemiAudio::VorbisComment.new
    comments.size.should eq 0
    comments.numKeys.should eq 0

    comments << {"test", "foo"}
    comments.size.should eq 1
    comments.numKeys.should eq 1

    comments << {"test", "bar"}
    comments.size.should eq 2
    comments.numKeys.should eq 1

    comments["tEsT2"] = "lol"
    comments.size.should eq 3
    comments.numKeys.should eq 2

    comments["TEST"].size.should eq 2
    comments["test"][0].should eq "foo"
    comments["TEST"][1].should eq "bar"

    comments["TEST2"].size.should eq 1
    comments["test2"][0].should eq "lol"
  end

  it "Deletes comments" do
    comments = RemiAudio::VorbisComment.new
    comments.size.should eq 0
    comments.numKeys.should eq 0

    comments << {"test", "bad"}
    comments.size.should eq 1
    comments.numKeys.should eq 1

    comments << {"test", "bar"}
    comments.size.should eq 2
    comments.numKeys.should eq 1

    comments["test"][0].should eq "bad"
    comments["tESt"][1].should eq "bar"

    comments.delete("test", 0)
    comments.size.should eq 1
    comments.numKeys.should eq 1
    comments["TEST"][0].should eq "bar"

    comments.clear
    comments.size.should eq 0
    comments.numKeys.should eq 0

    comments << {"test", "foo"}
    comments << {"test", "bar"}
    comments << {"test", "baz"}
    comments << {"test2", "boobs"}
    comments.size.should eq 4
    comments.numKeys.should eq 2

    comments.delete("teSt")
    comments.size.should eq 1
    comments.numKeys.should eq 1
    comments["test2"].size.should eq 1
    comments["test2"][0].should eq "boobs"

    comments << {"test", "foo"}
    comments.delete("teSt")
    comments.size.should eq 1
    comments.numKeys.should eq 1
    comments["test2"].size.should eq 1
    comments["test2"][0].should eq "boobs"

    comments << {"test", "foo"}
    comments << {"test", "bar"}
    comments.delete("teSt", 1)
    comments.size.should eq 2
    comments.numKeys.should eq 2
    comments["test"].size.should eq 1
    comments["test"][0].should eq "foo"
    comments["test2"].size.should eq 1
    comments["test2"][0].should eq "boobs"

    comments.delete("not a key")

    expect_raises(KeyError) do
      comments.delete("not a key", 1)
    end
  end

  it "Writes comments" do
    comments = RemiAudio::VorbisComment.new
    comments << {"test", "foo"}
    comments << {"test", "bar"}

    io = IO::Memory.new
    comments.write(io)
    io.rewind

    comments = RemiAudio::VorbisComment.new(io)
    comments.size.should eq 2
    comments.numKeys.should eq 1

    comments["test"][0].should eq "foo"
    comments["tESt"][1].should eq "bar"
  end

  it "Writes comments (vendor string set)" do
    comments = RemiAudio::VorbisComment.new
    comments << {"test", "foo"}
    comments << {"test", "bar"}
    comments.vendor = "test vendor"

    io = IO::Memory.new
    comments.write(io)
    io.rewind

    comments = RemiAudio::VorbisComment.new(io)
    comments.size.should eq 2
    comments.numKeys.should eq 1
    comments.vendor.should eq "test vendor"

    comments["test"][0].should eq "foo"
    comments["tESt"][1].should eq "bar"
  end

  it "Handles framing bits" do
    comments = RemiAudio::VorbisComment.new
    comments << {"test", "foo"}
    comments << {"test", "bar"}

    io = IO::Memory.new
    comments.write(io, true)
    io.rewind

    expect_raises(RemiAudio::VorbisComment::Error) do
      comments = RemiAudio::VorbisComment.new(io)
    end

    io.rewind
    comments = RemiAudio::VorbisComment.new(io, true)
    comments.size.should eq 2
    comments.numKeys.should eq 1

    comments["test"][0].should eq "foo"
    comments["tESt"][1].should eq "bar"
  end
end
